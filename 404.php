<?php get_header(); ?>

<?php get_template_part('partials/masthead'); ?>

<div class="container">

    <!-- Use https://blendedwaxes.com/404 as an example -->

    <!-- Add image buttons below for top ~4 pages - one should be the homepage, PM to specify the others in specs -->
    <section class="imgbtns-404">
        <div class="row">
            <h2>Explore one of these pages instead:</h2>
            <div class="col-xxs-12 col-sm-4">
                <div class="suggested-pages">
                    <a href="<?php echo get_permalink(get_option( 'woocommerce_shop_page_id' ));?>" title="<?php echo get_the_title(get_option( 'woocommerce_shop_page_id' )); ?>">
                        <div class="suggested-pages__image <?php echo has_post_thumbnail(get_option( 'woocommerce_shop_page_id' ) ) ? '' : 'placeholder';?>">
                            <?php if ( has_post_thumbnail(get_option( 'woocommerce_shop_page_id' )) ) { ?>
                                <?php echo fx_get_image_tag( get_post_thumbnail_id(get_option( 'woocommerce_shop_page_id' )), '', 'full', '', get_the_title(get_option( 'woocommerce_shop_page_id' )) ); ?>
                            <?php } ?>
                        </div>
                        <div class="suggested-pages__title">
                            <h4><?php echo get_the_title(get_option( 'woocommerce_shop_page_id' )); ?></h4>
                        </div>
                    </a>
                </div>
                <!-- image button -->
            </div>
            <div class="col-xxs-12 col-sm-4">
                <div class="suggested-pages">
                    <a href="<?php echo get_permalink(50);?>" title="<?php echo get_the_title(50); ?>">
                        <div class="suggested-pages__image <?php echo has_post_thumbnail(50) ? '' : 'placeholder';?>">
                            <?php if ( has_post_thumbnail(50) ) { ?>
                                <?php echo fx_get_image_tag( get_post_thumbnail_id(50), '', 'full', '', get_the_title(50) ); ?>
                            <?php } ?>
                        </div>
                        <div class="suggested-pages__title">
                            <h4><?php echo get_the_title(50); ?></h4>
                        </div>
                    </a>
                </div>
                <!-- image button -->
            </div>
            <div class="col-xxs-12 col-sm-4">
                <div class="suggested-pages">
                    <a href="<?php echo get_permalink(598);?>" title="<?php echo get_the_title(598); ?>">   
                        <div class="suggested-pages__image <?php echo has_post_thumbnail(598) ? '' : 'placeholder';?>">
                            <?php if ( has_post_thumbnail(598) ) { ?>
                                <?php echo fx_get_image_tag( get_post_thumbnail_id(598), '', 'full', '', get_the_title(598) ); ?>
                            <?php } ?>
                        </div>  
                        <div class="suggested-pages__title">
                            <h4><?php echo get_the_title(598); ?></h4>
                        </div>
                    </a>
                </div>
                <!-- image button -->
            </div>
        </div>
    </section>

    <section class="links-404">
        <div class="row">
            <div class="col-xxs-12 col-md-6">
                <div class="search-404">
                    <h4>Or, try searching our site:</h4>
                    <?php get_search_form(); ?>
                </div>
            </div>
            <div class="col-xxs-12 col-md-6">
                <div class="contact-404">
                    <h4>Still can't find what you're looking for?</h4>
                    <a href="#" class="btn">Contact Us Today!</a>
                </div>
            </div>
        </div>
    </section>

</div>

<?php get_footer(); 