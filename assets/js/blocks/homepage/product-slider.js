var FX = ( function( FX, $ ) {


	$( () => {
		FX.HomepageProduct.init()
	})


	FX.HomepageProduct= {
		$slider: null,

		init() {
			$('.product-slider').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        variableWidth: true,
        arrows: false,
        autoplay: false,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 9999,
                settings: "unslick"
            },
            {
              breakpoint: 1200,
              settings:  {
                slidesToScroll: 1,
                infinite: true,
              }
            }
          ]
      });
		}
	}

	

	return FX

} ( FX || {}, jQuery ) )