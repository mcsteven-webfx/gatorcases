var FX = ( function( FX, $ ) {


	$( () => {
		FX.HomepageTestimonial.init()
	})


	FX.HomepageTestimonial= {
		$slider: null,

		init() {
			$('.testimonial-slider').slick({
          dots: false,
          infinite: true,
          speed: 300,
          slidesToShow: 1,
		  //autoplay: true,
  		  autoplaySpeed: 2000,
		  responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				arrows: false
			  }
			}
		  ]
        });
      }
    }

	

	return FX

} ( FX || {}, jQuery ) )