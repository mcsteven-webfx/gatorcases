var FX = ( function( FX, $ ) {


	$( () => {
		FX.Accordion.init()
	})


	FX.Accordion = {
		init() {
			$(".accordion-title").click(function() {
			  $(this).parent().toggleClass('open');
			  $(this).next().slideToggle();
			  $('.accordion-list-item').not($(this).parent()).removeClass('open');
			  $('.accordion-content').not($(this).next()).slideUp();
			});

			$('.accordion-title').click(function() {
				$('.testimonial-slider').slick('slickNext');
			  });
		},

	}

	

	return FX

} ( FX || {}, jQuery ) )