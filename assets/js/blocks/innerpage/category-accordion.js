var FX = ( function( FX, $ ) {


	$( () => {
		FX.categoryAccordion.init()
	})


	FX.categoryAccordion = {
		init() {

			$(document).on('facetwp-loaded', fx_add_facet_labels);

			function fx_add_facet_labels() {
				$('.facetwp-facet').each(function() {
					if ($(this).get(0).childElementCount > 0) {
						$(this).parents('.cat-list-item').show();
					} else {
						$(this).parents('.cat-list-item').hide();
					}

					$(this).find('.facetwp-depth').prev().hide();

				});

			}

            $(".cat-list-item:nth-child(1)").addClass('open');
			$(".cat-list-item:nth-child(1) .cat-content").show();
			$(".cat-title").click(function() {
			  $(this).parent().toggleClass('open');
			  $(this).next().slideToggle();
			  $('.cat-list-item').not($(this).parent()).removeClass('open');
			  $('.cat-content').not($(this).next()).slideUp();
			});
		},

	}



	return FX

} ( FX || {}, jQuery ) )
