var FX = ( function( FX, $ ) {


	$( () => {
		FX.InnerpageProduct.init()
	})


	FX.InnerpageProduct= {
		$slider: null,

		init() {
      $('.product-big').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        dots: true,
        asNavFor: '.product-thumb'
      });
      $('.product-thumb').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product-big',
        arrows: false,
        dots: false,
        focusOnSelect: true,
        vertical: true,
        verticalSwiping: true,
        responsive: [
          {
            breakpoint: 767,
            settings: {
              vertical: false,
              verticalSwiping: false
            }
          }
        ]
      });

      if ( $('body').hasClass('single-product') ) {
        $('.print-product').click(function(){
            window.print();
            return false;
        });
      }
          
		}
	}

	

	return FX

} ( FX || {}, jQuery ) )