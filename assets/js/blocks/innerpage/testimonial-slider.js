var FX = ( function( FX, $ ) {


	$( () => {
		FX.HomepageTestimonial.init()
	})


	FX.HomepageTestimonial= {
		$slider: null,

		init() {
		$('.testimonial-slider').slick({
          dots: false,
          infinite: true,
          speed: 300,
          slidesToShow: 1,
		  //autoplay: true,
  		  autoplaySpeed: 2000,
		  asNavFor: '.testimonial-pict-slider',
		  responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				arrows: false
			  }
			}
		  ]
        });
		$('.testimonial-pict-slider').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			arrows: false,
			fade: true,
			asNavFor: '.testimonial-slider',
		  });
      }
    }

	

	return FX

} ( FX || {}, jQuery ) )