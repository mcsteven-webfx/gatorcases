<section class="wysiwyg">
	<div class="container">
		<div class="row">
			<div class="<?php echo is_single() ? 'col-xs-12' : 'col-md-8 col-md-offset-2';?>">
				<?php the_field( 'content' ); ?>
			</div>
		</div>
	</div>
</section>