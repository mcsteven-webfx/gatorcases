<section class="wysiwyg">

	<div class="container">
		<div class="row">
			<div class="<?php echo is_single() ? 'col-xs-12' : 'col-md-6 col-md-offset-3';?> text-center">
				<?php the_field( 'content' ); ?>
			</div>
		</div>
	</div>
	
</section>