<!-- Award section -->
<section class="award-section">
    <div class="container">
        <div class="award-title text-center"><?php echo get_field('awards_title'); ?></div>

        <?php if( have_rows( 'awards_list' ) ): ?>
        <ul>
        <?php 
                $skip_lazy = true; 
                while( have_rows( 'awards_list' ) ): the_row();
        ?>
            <li>
                <?php echo fx_get_image_tag( get_sub_field( 'image' ), '', 'full', $skip_lazy, ''); ?>
            </li>
            <?php
                $skip_lazy = false; 
                endwhile;
            ?>
        </ul>
        <?php endif; ?>
    </div>
</section>