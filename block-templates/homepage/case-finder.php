<?php wp_enqueue_script( 'fx-case-finder-js' ); ?>

<!-- video section -->
<section class="video-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 hidden-md-down">
                <div class="video-side-pict">
                    <ul>
                        <?php
                            $image_1 = get_field('case_finder_image_1');
                            $image_2 = get_field('case_finder_image_2');
                        ?>
                        <li><?php echo fx_get_image_tag( $image_1, '', 'full', '', '' ); ?></li>
                        <li><?php echo fx_get_image_tag( $image_2, '', 'full', '', '' ); ?></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="guiter-content">
                    <div class="guitar-vid-img hidden-lg">
                        <?php
                            $image_3 = get_field('case_finder_image_3');
                            $video = get_field('case_finder_video');
                        ?>
                        <?php echo fx_get_image_tag( $image_3, '', 'full', '', '' ); ?>
                    </div>
                    <div class="video-container-container hidden-md-down">
                        <div class="video-container">
                            <video id="v1" class="hero-vid" autoplay="" muted="" playsinline="" loop="">
                                <source src="<?php echo $video; ?>" type="video/mp4">
                            </video>
                        </div>
                    </div>
                    <div class="video-form">
                        <h3>Find Your Perfect Case</h3>
                        <style>
                            select {
                                width:100%;
                                padding:10px;
                            }
                        </style>
                        <form id="search-case-form" action="/case-advisor-results/" method="get">
                            <div class="form-col">
                                <?php
                                    $case_finder_categories = fx_get_case_finder_categories();
                                ?>
                                <select id="case-finder_category">
                                    <option value="">Select Category</option>
                                    <optgroup label="Guitars">
                                        <?Php foreach ( $case_finder_categories as $case_category ) : ?>
                                            <?php $guitar_name = $case_category["Type_Lookup"]; ?>
                                            <?php if(strpos($guitar_name, 'Guitar') !== false || strpos($guitar_name, 'Ukulele') !== false ) : ?>
                                                <option value="<?php echo $case_category["CA_Category"]; ?>"><?php echo $case_category["CA_Category"]; ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </optgroup>
                                    <?Php foreach ( $case_finder_categories as $case_category ) : ?>
                                        <?php $guitar_name = $case_category["Type_Lookup"]; ?>
                                        <?php if(strpos($guitar_name, 'Guitar') === false && strpos($guitar_name, 'Ukulele') === false ) : ?>
                                            <option value="<?php echo $case_category["CA_Category"]; ?>"><?php echo $case_category["CA_Category"]; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-col">
                                <select id="case-finder_manufacturer" disabled>
                                    <option value="">Select Manufacturer</option>
                                </select>
                            </div>
                            <div class="form-col">
                                <select id="case-finder_model" disabled>
                                    <option value="">Select Model</option>
                                </select>
                            </div>
                            <div class="form-col">
                                <a href="#" id="search-case-submit" style="pointer-events:none;" class="btn btn-primaru btn-block results-finder">Search</a>
                                <p class="text-center">Or <a href="#">Search By Dimension</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
