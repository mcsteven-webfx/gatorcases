<section class="product-section">
    <div id="product-finder-results" class="container">
    </div>
</section>
<section class="product-section">
    <div class="container">
        <h2><?php echo get_field('featured_products_title'); ?></h2>
        <?php
            $args = array(
                'post_type' => 'product',
                'posts_per_page' => 4,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_visibility',
                        'field'    => 'name',
                        'terms'    => 'featured',
                    ),
                )
            );
            $f_products = new WP_Query($args);

            if ( $f_products->have_posts() ) :
        ?>
            <div class="product-slider">
                <?php  
                    while( $f_products->have_posts() ) : $f_products->the_post();
                        $cat = get_the_terms( get_the_ID(), 'product_cat');
                        global $product;
                ?>
                    <div class="product-item">
                        <div class="product-content">
                            <div class="product-box">
                                <div class="product-title"><?php echo $cat[0]->name; ?></div>
                                <div class="product-img-box">
                                    <a href="#"><i class="icon-heart"></i></a>
                                    <div class="product-img">
                                        <a href="<?php echo get_permalink(); ?>">
                                            <?php echo fx_get_image_tag( get_post_thumbnail_id(), '', 'full', '', get_the_title() ); ?>
                                        </a>
                                        <div class="product-box-bttn text-center"><a href="<?php echo get_permalink(); ?>" class="btn btn-secondary">View Product</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-description">
                                <p><?php echo get_the_title(); ?></p>
                                <div class="compare-link">
                                    <?php
                                        remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_add_to_cart', 10);
                                        do_action( 'woocommerce_after_shop_loop_item' );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
        <?php endif; ?>
    </div>
</section>

