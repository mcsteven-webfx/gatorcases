 <!-- Image Button -->
 <section class="image-button">
    <div class="container">
        <h2><?php echo get_field('gator_guides_title'); ?></h2>
        <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => -1,
                'order' => 'ASC',
                'meta_query' => array(
                    array(
                        'key' => 'featured',
                        'value' => 1
                    )
                )
            ); 
            $blogs = new WP_Query($args);
        ?>
        <?php if ( $blogs->have_posts() ) : ?>
            <div class="row">
                <?php while( $blogs->have_posts() ) : $blogs->the_post(); ?>
                    <div class="col-sm-4">
                        <div class="bttn-item">
                            <a href="<?php echo get_permalink(); ?>">
                                <div class="bttn-img">
                                    <?php echo fx_get_image_tag(get_post_thumbnail_id(), '', 'full', '', get_the_title() ); ?>
                                    <div class="category">
                                        <?php
                                            $cat = get_the_category(); 
                                            echo $cat[0]->cat_name;
                                        ?>
                                    </div>
                                </div>
                                <div class="bttn-text"><?php echo get_the_title(); ?></div>
                            </a>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</section>