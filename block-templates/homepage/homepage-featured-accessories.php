<section class="one-third-section">
    <div class="container">
        <?php 
            if( have_rows( 'featured_accessories' ) ):
                while( have_rows( 'featured_accessories' ) ): the_row();
        ?>
            <div class="one-thir-main clearfix">
                <?php
                    $image_pos = get_sub_field('image_position');
                    $position = "";
                    $text_pos = "";
                    $image_id = get_sub_field( 'image' );
                    $yt_url = get_sub_field('youtube_video');
                    $selection = get_sub_field('image__youtube_link');
                    $desc_pos = '';
                    if ( $image_pos === 'left' ) {
                        $position = 'left-side';
                        $text_pos = 'right-text-side';
                        $desc_pos = 'one-third-right-text';
                    } else {
                        $position = 'right-side';
                        $text_pos = 'clearfix';
                        $desc_pos = 'one-third-left-text';
                    }
                ?>
                <div class="one-thir-img <?php echo $position; ?>">

                    <?php if ( $selection === 'image' ) { ?>
                        <?php echo fx_get_image_tag($image_id, 'full-h-img ', 'masthead', '', '' ); ?>
                    <?php 
                        } else {
                            $video_id = explode("?v=", $yt_url);
                            $video_id = $video_id[1];
                            $thumbnail="https://img.youtube.com/vi/".$video_id."/maxresdefault.jpg";
                    ?>
                        <a class="external yt-popup" data-fancybox="vid-popup" href="<?php echo $yt_url; ?>" target="_blank" rel="noopener">
                            <img class="play-btn" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-btn.png">
                            <img class="yt-image" src="<?php echo $thumbnail; ?>" alt="">
                        </a>
                    <?php } ?>
                </div>
                <div class="one-third-text <?php echo $text_pos; ?>">
                    <div class="<?php echo $desc_pos; ?>">
                        <div class="one-third-wrapper">
                            <?php echo get_sub_field('description'); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php
                endwhile; 
            endif;
        ?>
    </div>
</section>