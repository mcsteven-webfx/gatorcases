<section class="home-product">
    <div class="container">
        <div class="home-product-text text-center hidden-lg">
            <?php
                global $post;
                $block_info = get_field('block_info');
            ?>
            <?php if( !empty($block_info['title']) ) : ?>
                <h2><?php echo $block_info['title']; ?></h2>
            <?php endif; ?>
            <?php if( !empty($block_info['description']) ) : ?>
                <p><?php echo $block_info['description']; ?></p>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-4">
                <div class="row">
                    <?php
                        $prod_link_1 = get_field('product_link_1'); 
                        if ( !empty( $prod_link_1 ) ) {
                    ?>
                        <div class="col-xxs-6 col-sm-12">
                            <div class="product-box">
                                <?php
                                    
                                    $link_1 = "";
                                    $link_title_1 = "";
                                    
                                    $target_link_1 = "";

                                    if ( !empty($prod_link_1['link']['url']) ) {
                                        $target_link_1 = '_blank';
                                        $link_1 = $prod_link_1['link']['url'];
                                        $link_title_1 = $prod_link_1['link']['title'];
                                    } else {
                                        $target_link_1 = '_self';
                                        $link_1 = '#';
                                        $link_title_1 = "";
                                    }
                                ?>
                                <a href="<?php echo $link_1; ?>" target="<?php echo $target_link_1; ?>">
                                    <div class="product-box-img">
                                        <?php echo fx_get_image_tag( $prod_link_1['image'], '', 'full', '', $prod_link_1['title'] ); ?>
                                    </div>
                                    <?php if( !empty($prod_link_1['title']) ) : ?>
                                        <div class="product-box-title"><?php echo $prod_link_1['title']; ?></div>
                                    <?php endif; ?>
                                    <div class="product-hover">
                                        <p><?php echo $prod_link_1['description']; ?></p>
                                        <span><?php echo $link_title_1; ?></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } // end of $prod_link_1 ?>

                    <?php
                        $prod_link_2 = get_field('product_link_2'); 
                        if ( !empty( $prod_link_2 ) ) {
                    ?>
                        <div class="col-xxs-6 col-sm-12">
                            <div class="product-box">
                                <?php
                                    $link_2 = "";
                                    $link_title_2 = "";
                                    $target_link_2 = "";

                                    if ( !empty($prod_link_2['link']['url']) ) {
                                        $target_link_2 = '_blank';
                                        $link_2 = $prod_link_2['link']['url'];
                                        $link_title_2 = $prod_link_2['link']['title'];
                                    } else {
                                        $target_link_2 = '_self';
                                        $link_2 = '#';
                                        $link_title_2 = "";
                                    }
                                ?>
                                <a href="<?php echo $link_2; ?>" target="<?php echo $target_link_2; ?>">
                                    <div class="product-box-img">
                                        <?php echo fx_get_image_tag( $prod_link_2['image'], '', 'full', '', $prod_link_2['title'] ); ?>
                                    </div>
                                    <?php if( !empty($prod_link_2['title']) ) : ?>
                                        <div class="product-box-title"><?php echo $prod_link_2['title']; ?></div>
                                    <?php endif; ?>
                                    <div class="product-hover">
                                        <p><?php echo $prod_link_2['description']; ?></p>
                                        <span><?php echo $link_title_2; ?></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } // end of $prod__link_2 ?>
                </div>    
            </div>
            <div class="col-lg-3 col-sm-4">
                <div class="row">
                    <?php
                        $prod_link_3 = get_field('product_link_3'); 
                        if ( !empty( $prod_link_3 ) ) {
                    ?>
                        <div class="col-xxs-6 col-sm-12">
                            <div class="product-box">
                                <?php
                                    $link_3 = "";
                                    $link_title_3 = "";
                                    $target_link_3 = "";

                                    if ( !empty($prod_link_3['link']['url']) ) {
                                        $target_link_3 = '_blank';
                                        $link_3 = $prod_link_3['link']['url'];
                                        $link_title_3 = $prod_link_3['link']['title'];
                                    } else {
                                        $target_link_3 = '_self';
                                        $link_3 = '#';
                                        $link_title_3 = "";
                                    }
                                ?>
                                <a href="<?php echo $link_3; ?>" target="<?php echo $target_link_3; ?>">
                                    <div class="product-box-img">
                                        <?php echo fx_get_image_tag( $prod_link_3['image'], '', 'full', '', $prod_link_3['title'] ); ?>
                                    </div>
                                    <?php if( !empty($prod_link_3['title']) ) : ?>
                                        <div class="product-box-title"><?php echo $prod_link_3['title']; ?></div>
                                    <?php endif; ?>
                                    <div class="product-hover">
                                        <p><?php echo $prod_link_3['description']; ?></p>
                                        <span><?php echo $link_title_3; ?></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } // end of $prod__link_3 ?>

                    <?php
                        $prod_link_4 = get_field('product_link_4'); 
                        if ( !empty( $prod_link_4 ) ) {
                    ?>
                    <div class="col-xxs-6 col-sm-12">
                        <div class="product-box">
                            <?php
                                $link_4 = "";
                                $link_title_4 = "";
                                
                                $target_link_4 = "";

                                if ( !empty($prod_link_4['link']['url']) ) {
                                    $target_link_4 = '_blank';
                                    $link_4 = $prod_link_4['link']['url'];
                                    $link_title_4 = $prod_link_4['link']['title'];
                                } else {
                                    $target_link_4 = '_self';
                                    $link_4 = '#';
                                    $link_title_4 = "";
                                }
                            ?>
                            <a href="<?php echo $link_4; ?>" target="<?php echo $target_link_4; ?>">
                                <div class="product-box-img">
                                    <?php echo fx_get_image_tag( $prod_link_4['image'], '', 'full', '', $prod_link_4['title'] ); ?>
                                </div>
                                <?php if( !empty($prod_link_4['title']) ) : ?>
                                    <div class="product-box-title"><?php echo $prod_link_4['title']; ?></div>
                                <?php endif; ?>
                                <div class="product-hover">
                                    <p><?php echo $prod_link_4['description']; ?></p>
                                    <span><?php echo $link_title_4; ?></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php } // end of $prod__link_4 ?>
                </div>    
            </div>
            <div class="col-xxs-6 col-sm-4 col-lg-3 hidden-md-down">
                <div class="home-product-text text-center">
                    <?php if ( !empty($block_info['title']) ) : ?>
                        <h2><?php echo $block_info['title']; ?></h2>
                    <?php endif; ?>
                    <?php if ( !empty($block_info['description']) ) : ?>
                        <p><?php echo $block_info['description']; ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-3 col-sm-4">
                <div class="row">
                    <?php
                        $prod_link_5 = get_field('product_link_5'); 
                        if ( !empty( $prod_link_5 ) ) {
                    ?>
                    <div class="col-xxs-6 col-sm-12">
                        <div class="product-box">
                            <?php
                                $link_5 = "";
                                $link_title_5 = "";
                                $target_link_5 = "";

                                if ( !empty($prod_link_5['link']['url']) ) {
                                    $target_link_5 = '_blank';
                                    $link_5 = $prod_link_5['link']['url'];
                                    $link_title_5 = $prod_link_5['link']['title'];
                                } else {
                                    $target_link_5 = '_self';
                                    $link_5 = '#';
                                    $link_title_5 = "";
                                }
                            ?>
                            <a href="<?php echo $link_5; ?>" target="<?php echo $target_link_5; ?>">
                                <div class="product-box-img">
                                    <?php echo fx_get_image_tag( $prod_link_5['image'], '', 'full', '', $prod_link_5['title'] ); ?>
                                </div>
                                <?php if( !empty($prod_link_5['title']) ) : ?>
                                    <div class="product-box-title"><?php echo $prod_link_5['title']; ?></div>
                                <?php endif; ?>
                                <div class="product-hover">
                                    <p><?php echo $prod_link_5['description']; ?></p>
                                    <span><?php echo $link_title_5; ?></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php } // end of $prod_link_5 ?>

                    <?php
                        $prod_link_6 = get_field('product_link_6'); 
                        if ( !empty( $prod_link_6 ) ) {
                    ?>
                    <div class="col-xxs-6 col-sm-12">
                        <div class="product-box">
                            <?php
                                $link_6 = "";
                                $link_title_6 = "";
                                $target_link_6 = "";

                                if ( !empty($prod_link_6['link']['url']) ) {
                                    $target_link_6 = '_blank';
                                    $link_6 = $prod_link_6['link']['url'];
                                    $link_title_6 = $prod_link_6['link']['title'];
                                } else {
                                    $target_link_6 = '_self';
                                    $link_6 = '#';
                                    $link_title_6 = "";
                                }
                            ?>
                            <a href="<?php echo $link_6; ?>" target="<?php echo $target_link_6; ?>">
                                <div class="product-box-img">
                                    <?php echo fx_get_image_tag( $prod_link_6['image'], '', 'full', '', $prod_link_6['title'] ); ?>
                                </div>
                                <?php if( !empty($prod_link_6['title']) ) : ?>
                                    <div class="product-box-title"><?php echo $prod_link_6['title']; ?></div>
                                <?php endif; ?>
                                <div class="product-hover">
                                    <p><?php echo $prod_link_6['description']; ?></p>
                                    <span><?php echo $link_title_6; ?></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php } // end of $prod_link_6 ?>
                </div>    
            </div>
        </div>
    </div>
</section>