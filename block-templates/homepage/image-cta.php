<?php if( have_rows( 'image_cta_lists' ) ): ?>
    <section class="box-button">
        <div class="container">
            <div class="row">
                <?php while( have_rows( 'image_cta_lists' ) ): the_row(); ?>
                    <div class="col-lg-6">
                        <div class="box-content">
                            <div class="box-img clearfix">
                                <?php
                                    $image_id = get_sub_field('image');
                                    $title = get_sub_field( 'title' );
                                ?>
                                <?php echo fx_get_image_tag( $image_id, 'masthead-slide__img', 'masthead', '', $title ); ?>
                            </div>
                            <div class="box-text">
                                <h3><?php echo $title; ?></h3>
                                <?php echo get_sub_field('description'); ?>
                                <?php if( $button = get_sub_field( 'link' )  ): ?>
                                    <a
                                        class="btn btn-primary"
                                        href="<?php echo esc_url( $button['url'] ); ?>"
                                        target="<?php echo $button['target'] ? $button['target'] : '_self';?>"
                                    >
                                        <?php echo $button['title']; ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>