<?php if( have_rows( 'slides' ) ): ?>
    <section class="banner">
        <div class="masthead-slider">
            <div class="js-masthead-homepage-slider">

                <?php $skip_lazy = true; // skip lazy loading for first image to improve paint times
                while( have_rows( 'slides' ) ): the_row(); ?>
                    <div class="slider-item">
                        <div class="container">
                            <div class="slider-content">
                                <div class="hero-img">
                                    <?php echo fx_get_image_tag( get_sub_field( 'background_image' ), 'masthead-slide__img', 'masthead', $skip_lazy, get_sub_field('headline') ); ?>
                                </div>
                                <div class="hero-text text-center">
                                    <h1><?php echo get_sub_field('headline'); ?></h1>
                                    <h2><?php echo get_sub_field('sub_headline'); ?></h2>
                                    <?php if( $button = get_sub_field( 'button' )  ): ?>
                                        <a
                                            class="btn btn-primary hidden-xs-down"
                                            href="<?php echo esc_url( $button['url'] ); ?>"
                                            target="<?Php echo $button['target'] ? $button['target'] : '_self';?>"
                                        >
                                            <?php echo $button['title']; ?>
                                        </a>
                                    <?php endif; ?>
                                    <div class="hidden-sm-up banner-bttn">
                                        <a href="#" class="btn btn-primary">Shop By Category</a>
                                        <a href="#" class="btn btn-secondary">Shop By Artist</a>
                                    </div>
                                </div>
                            </div>
                        </div>     
                    </div>
                <?php $skip_lazy = false;
                endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
