<section class="testimonial-section">
    <div class="container">
        <div class="testimonial-content">
            <div class="testimonial-bg">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/testimonial-bg.jpg" alt="">
            </div>
            <div class="testimonial-title text-center"><?php echo get_field('testimonial_subhead');?></div>
            <h2 class="text-center"><?php echo get_field('testimonial_heading');?></h2>

            <?php
                $args = array(
                    'post_type' => 'testimonial',
                    'posts_per_page' => -1,
                    'orderby'       =>  'date',
                    'order'         =>  'DESC',
                    'meta_query'    =>  array(
                        array(
                            'key'       => 'featured',
                            'value'     => 1
                        ),
                    )
                );

                $testimonials = new WP_Query($args);
            ?>

            <?php if ( $testimonials->have_posts() ) : ?>
                <div class="testimonial-slider">
                    <?php while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
                    <div class="testimonila-item">
                        <div class="testimonial-text">
                            <?php echo get_field('testimonial_content', get_the_ID() ); ?>
                        </div>
                    </div>
                    <?php endwhile; wp_reset_postdata() ?>
                </div>
            <?php endif; ?>
            <div class="testimonial-bttn text-center">
                <?php if( $button = get_field( 'testimonial_link' )  ): ?>
                    <a
                        class="btn btn-secondary"
                        href="<?php echo esc_url( $button['url'] ); ?>"
                        target="<?Php echo $button['target'] ? $button['target'] : '_self';?>"
                    >
                        <?php echo $button['title']; ?>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>