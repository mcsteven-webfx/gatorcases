<section class="accordion-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="text-center"><?php echo get_field('accordion_title'); ?></h2>
                <?php if ( have_rows('inner_accordion') ) : ?>
                    <div class="accordion">
                        <div class="accordion-list">
                            <?php while( have_rows( 'inner_accordion' ) ): the_row(); ?>
                                <div class="accordion-list-item">
                                    <h5 class="accordion-title"><?php echo get_sub_field('title'); ?></h5>
                                    <div class="accordion-content">
                                        <?php echo get_sub_field('content'); ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>