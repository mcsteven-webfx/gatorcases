<section class="grey-cta-form">
    <div class="container">
        <div class="text-center grey-cta-title">
            <h2><?php echo get_field('case_finder_shop_title'); ?></h2>
            <?php echo get_field('case_finder_shop_description'); ?>
        </div>
        <form action="" method="get">
            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="form-col">
                        <select name="category">
                            
                            <option>Select Category</option>
                            <?php
                                $product_terms = get_terms(
                                    'product_cat',
                                    array(
                                        'hide_empty' => true,
                                        'orderby' => 'count'
                                    )
                                );
                            ?>
                            <?Php foreach ( $product_terms as $product_term ) { ?>
                                <option value="<?php echo $product_term->slug; ?>"><?php echo $product_term->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="form-col">
                        <select name="brand">
                            <option>Select Manufacturer</option>
                            <?php
                                $product_terms = get_terms(
                                    'product_brands',
                                    array(
                                        'hide_empty' => true,
                                        'orderby' => 'count'
                                    )
                                );
                            ?>
                            <?Php foreach ( $product_terms as $product_term ) { ?>
                                <option value="<?php echo $product_term->slug; ?>"><?php echo $product_term->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="form-col">
                        <select name="model">
                            <option>Select Model</option>
                            <?php
                                $product_terms = get_terms(
                                    'product_model_type',
                                    array(
                                        'hide_empty' => true,
                                        'orderby' => 'count'
                                    )
                                );
                            ?>
                            <?Php foreach ( $product_terms as $product_term ) { ?>
                                <option value="<?php echo $product_term->slug; ?>"><?php echo $product_term->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="form-bttn">
                        <input class="btn btn-primary text-center"  value="Search">
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>