<section class="cta">
    <div class="container">
        <div class="cta-content">
            <?php
                $image_id = get_field('cta_background_image');
                $title = get_field('cta_head_title');
                $desc = get_field('cta_description');
                $link = get_field('cta_bg_link');
            ?>
            <div class="cat-img">
                <?php echo fx_get_image_tag( $image_id , '', 'full', '', $title ); ?>
            </div>
            <div class="cta-text text-center">
                <h1><?php echo $title; ?></h1>
                <p><?php echo $desc; ?></p>
                
                <?php if ( $link ) : ?>
                <a 
                    href="<?php echo $link['url']; ?>"
                    class="btn btn-primary" 
                    target="<?php echo $link['target'] ? $button['target'] : '_self';?>"
                >
                    <?php echo $link['title'];?>
                </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>