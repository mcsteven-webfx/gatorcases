<section class="testimonial-section innerpage-testimonial">
    <div class="container">
        <div class="testimonial-content">
            <div class="testimonial-bg">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/testimonial-bg.jpg" alt="<?php echo get_field('customer_testimonials_title'); ?>">
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <?php if ( get_field('customer_testimonials_title') ) : ?>
                        <h2 class="text-center hidden-md-up"><?php echo get_field('customer_testimonials_title'); ?></h2>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6">
                            <?php
                                $cat = get_field('select_testimonial_category');

                                $args = array(
                                    'post_type'         => 'testimonial',
                                    'posts_per_page'    => -1,
                                    'order' => 'ASC',
                                    'tax_query'     =>  array(
                                        array(
                                            'taxonomy'  => 'testimonial_category',
                                            'field'      => 'term_id',
                                            'terms'     => $cat,
                                            'operator'  => 'IN'
                                        )
                                    )
                                );
                                $testimonials = new WP_Query($args);
                            ?>
                            <div class="testimonial-pict-slider">
                                <?php 
                                    while ( $testimonials->have_posts() ) : $testimonials->the_post();
                                    $ID = get_the_ID();
                                ?>
                                <div class="testimonial-pict-item">
                                    <div class="testimonial-pict">
                                        <?php echo fx_get_image_tag( get_post_thumbnail_id() , '', 'full', '', get_the_title() ); ?>
                                    </div>
                                </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-lg-pull-6">
                            <div class="flex-height">
                                <?php if ( get_field('customer_testimonials_title') ) : ?>
                                    <h2 class="text-center hidden-sm-down"><?php echo get_field('customer_testimonials_title'); ?></h2>
                                <?php endif; ?>
                                    <div class="testimonial-slider">
                                        <?php 
                                            while ( $testimonials->have_posts() ) : $testimonials->the_post();
                                            $category = get_the_terms( $ID , 'testimonial_category' );
                                        ?>
                                            <div class="testimonila-item">
                                                <div class="testimonial-text">
                                                    <?php echo get_field('testimonial_content', $ID  ); ?>
                                                    <div class="author"><?php echo get_field('client_name', $ID ); ?><span><?php echo $category[0]->name;?></span></div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                
                                <?php if ( $link = get_field('testimonial_link') ) : ?>
                                    <div class="testimonial-bttn text-center">
                                        <a 
                                            href="<?php echo $link['url']; ?>"
                                            class="btn btn-primary" 
                                            target="<?php echo $link['target'] ? $button['target'] : '_self';?>"
                                        >
                                            <?php echo $link['title'];?>
                                        </a>
                                    </div>
                                 <?php endif; ?>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>