<?php if( have_rows( 'inner_block_gallery' ) ): ?>
    <section class="innerproduct-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="innerproduct-content clearfix">
                        <div class="product-big">
                            <?php while( have_rows( 'inner_block_gallery' ) ): the_row(); ?>
                                <div class="product-big-item">
                                    <?php
                                        $gallery_select = get_sub_field('gallery_video__image');
                                        $image = get_sub_field('image');
                                        $video = get_sub_field('gallery_video_url');
                                    ?>

                                    <?php if ( $gallery_select === 'image' ) { ?>
                                        <a class="external" data-fancybox="gallery-popup" href="<?php echo $image; ?>" target="_blank" rel="noopener">
                                            <img class="image" src="<?php echo $image; ?>">
                                        </a>
                                        <?php ?>
                                    <?php 
                                        } else {
                                            $video_id = explode("?v=", $video);
                                            $video_id = $video_id[1];
                                            $thumbnail="https://img.youtube.com/vi/".$video_id."/maxresdefault.jpg";
                                    ?>
                                        <a class="external" data-fancybox="gallery-popup" href="<?php echo $video; ?>" target="_blank" rel="noopener">
                                            <img class="play-btn" src="<?php echo get_template_directory_uri(); ?>/assets/img/play-btn.png">
                                            <img class="yt-image image" src="<?php echo $thumbnail; ?>" alt="">
                                        </a>
                                    <?php } ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                        <div class="product-thumb">
                            <?php while( have_rows( 'inner_block_gallery' ) ): the_row(); ?>
                                <div class="product-thumb-item">
                                    <div class="product-border">
                                        <?php if ( get_sub_field('gallery_video__image') === 'image' ) { ?>
                                            <img src="<?php echo get_sub_field('image'); ?>">
                                        <?php 
                                            } else {
                                                $video_id = explode("?v=", get_sub_field('gallery_video_url'));
                                                $video_id = $video_id[1];
                                                $thumbnail="https://img.youtube.com/vi/".$video_id."/maxresdefault.jpg";
                                        ?>
                                            <img class="yt-image" src="<?php echo $thumbnail; ?>" alt="">
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>