<!-- Footer -->
<footer class="page-footer">
    <div class="top-footer">
        <div class="container clearfix">
            <div class="left-footer">
                <div class="footer-logo">
                    <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/footer-logo.png" alt="<?php echo get_bloginfo('name')?>"></a>
                </div>
                <ul class="social hidden-md-up clearfix">
                    <li>
                        <a href="#" target="_blank"><i class="icon-facebook"></i></a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="icon-instragram"></i></a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="icon-youtube"></i></a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="icon-linkedin"></i></a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="icon-twitter"></i></a>
                    </li>
                </ul>
                <ul class="footer-address">
                <?php
                    // gets contact information set in Theme Settings
                    $address    = fx_get_client_address();
                    $email      = fx_get_client_email( true );
                    $phone      = fx_get_client_phone_number();
                    $phone_link = fx_get_client_phone_number( true );
                    $phone_2    = fx_get_client_phone_number_2();
                    $phone_link_2 = fx_get_client_phone_number_2(true);
                ?>
                    <li><?php echo $address; ?></li>
                    <li><a href="<?php echo esc_url( sprintf( 'tel:%s', $phone_link ) ); ?>"><?php echo $phone; ?></a></li>
                    <li><a href="<?php echo esc_url( sprintf( 'tel:%s', $phone_link_2 ) ); ?>"><?php echo $phone_2; ?></a></li>
                </ul>
            </div>
            <div class="right-footer">
                <?php
                    // Output the footer navigation
                    wp_nav_menu(
                        [
                            'container'         => 'div',
                            'container_class'   => 'footer-nav',
                            'depth'             => 1,
                            'theme_location'    => 'footer_menu',
                        ]
                    );
                ?>
                <div class="back-top">
                    <a href="javascript:void(0)" id="back-to-top" class="btn"> Back to the top</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer">
        <div class="container clearfix">
            <div class="copyright">
                <ul class="clearfix">
                    <li><a href="<?php echo get_permalink(305);?>">Site Credits</a></li>
                    <li><a href="<?php echo get_permalink(307);?>">Sitemap</a></li>
                    <li><a href="<?php echo get_permalink(3);?>">Privacy Policy</a></li>
                    <li>Copyright &copy; <?php get_the_date('Y'); ?>. All Rights Reserved.</li>
                </ul>
            </div>
            <div class="bottom-social hidden-sm-down">
                <ul class="social clearfix">
                    <li>
                        <a href="#" target="_blank"><i class="icon-facebook"></i></a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="icon-instragram"></i></a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="icon-youtube"></i></a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="icon-linkedin"></i></a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="icon-twitter"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Footer -->

<!-- Back To Top Icon area
<button class="back-to-top js-back-to-top" type="button">
<span class="back-to-top__label">Back to top</span>
<i class="icon-arrow-up"></i>
</button>
-->

<script id="omacro-widget-buynow" src="https://buynow.omacro.com/embedded/bn.loader.js?company=a4ee107e-eaba-4983-8a3f-b834f2951bbb&country=auto&validate=false&debug=false" async></script>

<?php wp_footer(); ?>
</body>
</html>
