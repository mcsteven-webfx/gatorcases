<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin />
    <?php // Insert Google Fonts <link> here. Please use &display=swap in your URL! 
    ?>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <?php wp_body_open(); ?>
        <?php
            // gets client logo image set in Theme Settings
            $logo_id    = fx_get_client_logo_image_id(); 
            $home_url   = get_home_url();
        ?>

    <header class="page-header" id="page-header">
        <div class="container clearfix">
            <div class="top-header">
                <div class="left-header hidden-sm-down">
                    <ul class="clearfix">
                        <li><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-1.png" alt=""></li>
                        <li><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-2.png" alt=""></li>
                        <li><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-3.png" alt=""></li>
                    </ul>
                </div>
                <div class="logo">
                    <a href="<?php echo esc_url( $home_url ); ?>">
                        <?php echo fx_get_image_tag( $logo_id, 'logo', '', '', get_bloginfo('name')); ?>
                    </a>
                </div>
                <div class="scrolled-menu">
                    <?php 
                        // Output the ubermenu. Copy code from ubermenu settings in Wordpress and update here
                        ubermenu( 'main', 
                            array( 
                                'menu' => 22
                            )
                        ); 
                    ?>
                </div>
                <div class="tab-bttn hidden-xs-down hidden-md-up">
                    <a href="#" class="btn btn-primary">Shop By Category</a>
                    <a href="#" class="btn btn-secondary">Shop By Artist</a>
                </div>

                <div class="right-header">
                    <?php get_search_form(); ?>
                    <div class="cart-head hidden-sm-down">
                        <ul class="clearfix">
                            <li class="search-click float-search">
                                <i class="icon-search"></i>
                            </li>
                            <li>
                                <?php
                                    $loc_field = strip_tags(get_field( 'address', 'options' ));
                                    $location = preg_replace('/\s+/', '+', $loc_field);
                                ?>
                                <a href="https://www.google.com/maps/place/<?php echo $location; ?>" target="_blank"><i class="icon-location"></i></a>
                            </li>
                            <?php if ( !is_user_logged_in() && !is_account_page() ) : ?>
                                <li>
                                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
                                        <i class="icon-profile"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li class="whistlist-click">
                                <a href="#"><i class="icon-heart"></i></a>
                            </li>
                        </ul>
                    </div>
                    <?php wc_get_template_part( 'wishlist','popup'); ?>
                </div>
            </div>
        </div>

        <!-- nav-fixed -->
        <div class="nav-fixed hidden-md-up">
            <ul class="clearfix">
                <li class="search-click">
                    <i class="icon-search"></i><span>Search</span>
                </li>
                <li>
                    <a href="#"><i class="icon-location"></i><span>Locations</span></a>
                </li>
                <?php if ( !is_user_logged_in() && !is_account_page() ) : ?>
                    <li>
                        <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
                            <i class="icon-profile"></i><span>Login</span>
                        </a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="#"><i class="icon-heart"></i><span>Wishlists</span></a>
                </li>
                <li class="toggle-menu">
                    <?php ubermenu_toggle();?>
                </li>
            </ul>
        </div>
    </header>