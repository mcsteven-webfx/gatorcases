<?php get_header(); ?>

<?php get_template_part('partials/masthead'); ?>

<?php if( have_posts() ): ?>
    <section class="<?php echo get_post_type(); ?>-listing-container js-load-more-block" data-load-more-post-type="<?php echo get_post_type(); ?>">
        <div class="container">   
            <div class="row">  
                <div class="col-xs-12 <?php echo is_active_sidebar('sidebar') ? 'col-sm-8 col-md-9 loop-posts__content' : '';?>">   
                    <div class="<?php echo get_post_type(); ?>-listing js-load-more-posts row">    
                        <?php while( have_posts() ): the_post(); ?>
                            <?php get_template_part( 'partials/loop-content' ); ?>
                        <?php endwhile; ?>
                    </div>
                    <div class="<?php echo get_post_type(); ?>-listing__pagination row">
                        <div class="col-xxs-12">
                            <?php get_template_part( 'partials/pagination' ); ?> 
                        </div>
                    </div>        
                </div>
                <?php if ( is_active_sidebar('sidebar') ) : ?>
                    <div class="col-xs-12 col-sm-4 col-md-3 loop-posts__sidebar">
                        <div class="blog-search">
                            <?php get_search_form(); ?>
                        </div>  
                        <?php dynamic_sidebar('sidebar') ;?>
                    </div>
                <?php endif; ?>
            </div>
        </div> 
    </section>
<?php else: ?>
    Sorry, we couldn't find any posts.
<?php endif; ?>


<?php get_footer(); ?>