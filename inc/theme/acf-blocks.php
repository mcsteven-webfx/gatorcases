<?php

/**
 * Register FX blocks
 * 
 * fx_register_block() is, at its core, a wrapper function for acf_register_block_type with additional parameters for 
 * our supporting functionality 
 * 
 * @see Guru card: https://app.getguru.com/card/Tn9zzk8c/FX-ACF-Blocks
 * @see more info for acf_register_block_type(): https://www.advancedcustomfields.com/resources/acf_register_block_type/
 * 
 * Below is a reference for the parameters you can pass to fx_register_block(). You can also pass any setting from 
 * acf_register_block_type() to fx_register_acf_block().
 * 
 * Required arguments: "name", "title", and "template"
 * 
 */

// @todo — remove $reference_settings before launch
$reference_settings = [

    // required
    'name'                  => '', // (string) unique name to identify block (no spaces)
    'title'                 => '', // (string) display title for block
    'template'              => '', // (string) relative path of the template for block (e.g "/block-templates/innerpage/template.php")
    
    // optional
    'css'                   => '', // (string) block-specific stylesheet. Assumed to be in /themes/fx/assets/css, so use relative path (e.g. "homepage/homepage-block.css")
    'css_deps'              => [], // (array|string) CSS dependency handles. These will be loaded before block's stylesheet. Dependencies must already be registered
    'js'                    => '', // (string) block-specific script. Assumed to be in /themes/fx/assets/js, so use relative path (e.g. "homepage/homepage-block.js")
    'js_deps'               => [], // (array|string) JS dependency handles. These will be loaded before block's script. Dependencies must already be registered
    'description'           => '', // (string) short, useful description of block to indicate block's purpose
    'category'              => '', // (string) category for where block appears in Block Library
    'icon'                  => '', // (array|string) can be a dashicon or SVG image used to identify the block
    'keywords'              => '', // (array) terms to help find block in block editor
    'post_types'            => [], // (array) if declared, will restrict block to being available for only specified post types. Default is "page"
    'exclude_post_types'    => [], // (array) post types that block should NOT appear for
    'mode'                  => '', // (string) display mode for block when you add block in Block Editor
    'supports'              => '', // (associative array) features to support. See https://developer.wordpress.org/block-editor/developers/block-api/block-supports/
];




/**
 * General blocks
 * 
 * These blocks are intended to be used anywhere, including the homepage and innerpage.
 * 
 * Block template path: /themes/fx/block-templates/general
 * Stylesheet path:     /themes/fx/assets/css/general
 * Script path:         /themes/fx/assets/js/general
 * 
 */


/**
 * Create a "FX General Blocks" category in the block editor. Use "fx-general-blocks" as your "category" value in 
 * fx_register_block()
 * 
 */
fx_add_block_category( 'FX General Blocks', 'fx-general-blocks' );


/**
 * Plan WYSIWYG block for general usage
 * 
 */
fx_register_block(
    [
        'name'          => 'wysiwyg',
        'title'         => 'WYSIWYG Text Center',
        'template'      => 'general/wysiwyg.php',
        'description'   => 'A basic "What you see is what you get" editor.',
        'css'           => 'general/wysiwyg.css',
        'post_types'    => [],
    ]
);

fx_register_block(
    [
        'name'          => 'wysiwyg-left',
        'title'         => 'WYSIWYG Text Left',
        'template'      => 'general/wysiwyg-left.php',
        'description'   => 'A basic "What you see is what you get" editor.',
        'css'           => 'general/wysiwyg.css',
        'post_types'    => [],
    ]
);


/**
 * To avoid issues with CF7 assets, we're creating our own CF7 block. You shouldn't need to touch this section.
 *
 */
$cf7_settings = [
    'name'          => 'cf7-block',
    'title'         => 'CF7 Block',
    'template'      => 'general/cf7-block.php',
    'description'   => 'Adds a CF7 block to page',
    'css_deps'      => [ 'fx_choices_custom', 'contact-form-7' ],
    'js_deps'       => [ 'contact-form-7', 'wpcf7-recaptcha', 'google-recaptcha' ],
    'keywords'      => [ 'cf7', 'contact', 'form' ],
    'mode'          => 'edit',
    'post_types'    => [], // all post types,
];
$cf7_icon = WP_PLUGIN_DIR . '/contact-form-7/assets/icon.svg';
if( file_exists( $cf7_icon ) ) {
    $cf7_settings['icon'] = file_get_contents( $cf7_icon );
}
fx_register_block( $cf7_settings );

// @todo — add additional general blocks below with the "fx-general-blocks" category




/**
 * Homepage blocks
 * 
 * These blocks are intended to be used ONLY on the homepage.
 * 
 * Block template path: /themes/fx/block-templates/homepage
 * Stylesheet path:     /themes/fx/assets/css/homepage
 * Script path:         /themes/fx/assets/js/homepage
 * 
 */


/**
 * This is the main homepage "outer block." All other homepage blocks should be added within this block in the Block 
 * Editor and in block-templates/homepage/homepage-block.php
 * 
 */
fx_register_block(
    [
        'name'          => 'homepage-block',
        'title'         => 'Homepage',
        'template'      => 'homepage/homepage-block.php',
        'description'   => 'The main content block for the homepage',
        'mode'          => 'preview',
        'supports'      => [ 'jsx' => true ], // enables support for inner blocks
        'category'      => 'fx-homepage-blocks',
    ]
);

// @todo —  remove this block if not using a homepage masthead slider

fx_register_block(
    [
        'name'          => 'homepage-masthead-slider',
        'title'         => 'Homepage - Masthead Slider',
        'template'      => 'homepage/masthead-slider.php',
        'description'   => 'Slider block for the homepage masthead.',
        'css'           => 'homepage/masthead-slider.css',
        'css_deps'      => [ 'fx_slick' ],
        'js'            => 'homepage/masthead-slider.js',
        'js_deps'       => [ 'fx_slick' ],
        'category'      => 'fx-homepage-blocks'
    ]
);

//Homepage Image CTA
fx_register_block(
    [
        'name'          => 'homepage-image-cta',
        'title'         => 'Homepage - Image Call to Actions',
        'template'      => 'homepage/image-cta.php',
        'description'   => 'Block for Image Call to Actions',
        'css'           => 'homepage/box-button.css',
        'category'      => 'fx-homepage-blocks'
    ]
);

//Homepage - Case Finder
fx_register_block(
    [
        'name'          => 'homepage-case-finder',
        'title'         => 'Homepage - Case Finder Section',
        'template'      => 'homepage/case-finder.php',
        'description'   => 'Block for Case Finder Section',
        'css'           => 'homepage/video-section.css',
        // 'css_deps'      => ['fx_choices_custom'],
        'js'           => 'homepage/case-finder.js',
        'category'      => 'fx-homepage-blocks',
        
    ]
);

//Homepage - Featured Products
fx_register_block(
    [
        'name'          => 'homepage-featured-products',
        'title'         => 'Homepage - Featured Products',
        'template'      => 'homepage/featured-products.php',
        'description'   => 'Block for Featured Products',
        'css'           => 'homepage/product-category.css',
        'css_deps'       => ['fx_slick'],
        'js'           => 'homepage/product-slider.js',
        'js_deps'       => ['fx_slick'],
        'category'      => 'fx-homepage-blocks',
    ]
);

//Homepage - Awards
fx_register_block(
    [
        'name'          => 'homepage-awards',
        'title'         => 'Homepage - Awards',
        'template'      => 'homepage/awards.php',
        'description'   => 'Block for Awards',
        'css'           => 'homepage/awards.css',
        'category'      => 'fx-homepage-blocks',
    ]
);

//Homepage - Testimonials
fx_register_block(
    [
        'name'          => 'homepage-testimonials',
        'title'         => 'Homepage - Testimonials',
        'template'      => 'homepage/testimonials.php',
        'description'   => 'Block for Testimonials',
        'css'           => 'homepage/testimonial-section.css',
        'js'           => 'homepage/testimonial-slider.js',
        'category'      => 'fx-homepage-blocks',
    ]
);

//Homepage - Product Links
fx_register_block(
    [
        'name'          => 'homepage-product-links',
        'title'         => 'Homepage - Product Links',
        'template'      => 'homepage/homepage-product-links.php',
        'description'   => 'Block for Product Links',
        'css'           => 'homepage/home-product.css',
        'category'      => 'fx-homepage-blocks',
    ]
);

//Homepage - Featured Accessories
fx_register_block(
    [
        'name'          => 'homepage-featured-accessories',
        'title'         => 'Homepage - Image & Text Section',
        'template'      => 'homepage/homepage-featured-accessories.php',
        'description'   => 'Block for Image & Text Section',
        'css'           => 'homepage/one-third.css',
        'css_deps'      => ['fx_fancybox'],
        'js_deps'       => ['fx_fancybox'],
        'category'      => 'fx-homepage-blocks'
    ]
);

//Homepage - Gator Guides
fx_register_block(
    [
        'name'          => 'homepage-gator-guides',
        'title'         => 'Homepage - Gator Guides',
        'template'      => 'homepage/gator-guides.php',
        'description'   => 'Block for Gator Guides',
        'css'           => 'homepage/image-button.css',
        'category'      => 'fx-homepage-blocks'
    ]
);

//Homepage - Image Background CTA
fx_register_block(
    [
        'name'          => 'cta-background',
        'title'         => 'Image Background CTA',
        'template'      => 'innerpage/cta-image.php',
        'description'   => 'Block for Image Background CTA',
        'css'           => 'homepage/cta-section.css',
        'category'      => 'fx-homepage-blocks'
    ]
);


/**
 * Create a "FX Homepage Blocks" category in the block editor. Use "fx-homepage-blocks" as your "category" value in 
 * fx_register_block()
 * 
 */
fx_add_block_category( 'FX Homepage Blocks', 'fx-homepage-blocks' );

/**
 * Create a "FX Innerpage Blocks" category in the block editor. Use "fx-innerpage-blocks" as your "category" value in 
 * fx_register_block()
 * 
 */
 

 //Innerpage - customer Testimonials
fx_register_block(
    [
        'name'          => 'customer-testimonials',
        'title'         => 'Customer Testimonials',
        'template'      => 'innerpage/customer-testimonials.php',
        'description'   => 'Block for Customer Testimonials',
        'css'           => 'innerpage/testimonial-section.css',
        'js'            => 'innerpage/testimonial-slider.js',
        'js_deps'       => ['fx_slick'],
        'css_deps'      => ['fx_slick'],
        'category'      => 'fx-innerpage-blocks'
    ]
);

 //Innerpage - Inner Gallery
 fx_register_block(
    [
        'name'          => 'inner-gallery',
        'title'         => 'Gallery',
        'template'      => 'innerpage/gallery.php',
        'description'   => 'Block for Gallery',
        'css'           => 'innerpage/innerproduct-section.css',
        'js'            => 'innerpage/product-slick-slider.js',
        'js_deps'       => ['fx_slick','fx_fancybox'],
        'css_deps'      => ['fx_slick','fx_fancybox'],
        'category'      => 'fx-innerpage-blocks'
    ]
);

 //Innerpage - Accordion
 fx_register_block(
    [
        'name'          => 'inner-accordion',
        'title'         => 'Accordion',
        'template'      => 'innerpage/accordion.php',
        'description'   => 'Block for Accordion',
        'css'           => 'innerpage/accordion.css',
        'js'            => 'innerpage/accordion.js',
        'category'      => 'fx-innerpage-blocks'
    ]
);

 //Innerpage - Case Finder Shop Page
 fx_register_block(
    [
        'name'          => 'case-finder-shop',
        'title'         => 'Case Finder Shop Page',
        'template'      => 'innerpage/case-finder-shop.php',
        'description'   => 'Block for Case Finder Shop Page',
        'css'           => 'innerpage/cta-form.css',
        'category'      => 'fx-innerpage-blocks'
    ]
);


/**
 * Innerpage blocks
 * 
 * These blocks are intended to be used ONLY on innerpages
 * 
 * Block template path: /themes/fx/block-templates/innerpage
 * Stylesheet path:     /themes/fx/assets/css/innerpage
 * Script path:         /themes/fx/assets/js/innerpage
 * 
 */



// @todo — add additional innerpage blocks below with the "fx-innerpage-blocks" category


fx_add_block_category( 'FX Inner Blocks', 'fx-innerpage-blocks' );





