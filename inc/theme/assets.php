<?php

/**
 * Documentation on FX Assets:
 * https://app.getguru.com/card/ceEjzyKi/FX-Assets
 */


/**
 * Register and enqueue theme styles
 *
 * @return void
 */
add_action( 'wp_enqueue_scripts', 'fx_theme_styles' );
function fx_theme_styles() {
    $theme_dir = get_template_directory();
    $theme_url = get_template_directory_uri();

    $post = is_page() ? get_queried_object() : false;
    $wl_parent = "";

    if ( !empty($post) && is_a($post, 'WP_Post') ) {
        $wl_parent = (  93 === $post->post_parent );
    }

    /* Inline critical/above-the-fold stylesheets */

    fx_assets_add_stylesheet(
        [
            'handle'    => 'normalize',
            'src'       => $theme_url . '/assets/css/normalize.css',
            'inline'    => true,
            'priority'  => PHP_INT_MIN,
        ]
    );

    // TODO remove this stylesheet registration if not using ubermenu.
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-ubermenu',
            'src'       => plugins_url() . '/ubermenu/pro/assets/css/ubermenu.min.css',
            'inline'    => true,
        ]
    );

    // Styles that are required on every page
    fx_assets_add_stylesheet(
        [
            'handle'    => 'site-global',
            'src'       => $theme_url . '/assets/css/global.css',
            'inline'    => true,
        ]
    );

    fx_assets_add_stylesheet(
        [
            'handle'    => 'site-custom-properties',
            'src'       => $theme_url . '/assets/css/custom-properties.css',
            'inline'    => true,
        ]
    );

    // Header Styling
    fx_assets_add_stylesheet(
        [
            'handle'    => 'site-header',
            'src'       => $theme_url . '/assets/css/header.css',
            'enqueue'   => !is_admin(),
            'inline'    => ( is_woocommerce() ? false : true ),
         ]
    );


    /* Other theme styles, enqueued normally (not inline in header) */

    // Footer Styling
    fx_assets_add_stylesheet(
        [
            'handle'    => 'site-footer',
            'src'       => $theme_url . '/assets/css/footer.css',
            'enqueue'   => !is_admin()
        ]
    );

    // Single Product WYSIWYG Styling
    fx_assets_add_stylesheet(
        [
            'handle'    => 'single-product-wysiwyg',
            'src'       => $theme_url . '/assets/css/blocks/general/wysiwyg.css',
            'enqueue'   => is_product()
         ]
    );

    // Single Product Testimonial Section Styling
    fx_assets_add_stylesheet(
        [
            'handle'    => 'single-product-testimonial',
            'src'       => $theme_url . '/assets/css/blocks/homepage/testimonial-section.css',
            'enqueue'   => is_product()
         ]
    );


    // Posts-specific styling: blog singles, archives, search page
    fx_assets_add_stylesheet(
        [
            'handle'    => 'site-posts',
            'src'       => $theme_url . '/assets/css/posts.css',
            'enqueue'   => ( is_single() || is_home() || is_archive() || is_search() )
        ]
    );

    // Styles for only 404 page
    fx_assets_add_stylesheet(
        [
            'handle'    => 'site-404',
            'src'       => $theme_url . '/assets/css/404.css',
            'enqueue'   => is_404()
        ]
    );

    // Print Styles
    fx_assets_add_stylesheet(
        [
            'handle'    => 'site-print',
            'src'       => $theme_url . '/assets/css/print.css',
            'enqueue'   => !is_admin(),
            'media'     => 'print'
        ]
    );

     // Woologin Styles
     fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-woologin',
            'src'       => $theme_url . '/assets/css/blocks/innerpage/login.css',
            'enqueue'   => is_account_page()
        ]
    );

     // Product Single Styles
     fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-innerproduct',
            'src'       => $theme_url . '/assets/css/blocks/innerpage/innerproduct-section.css',
            'dependencies'  => [ 'fx_slick' ],
            'enqueue'   => is_product()
        ]
    );

    // Product Single Info Styles
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-productinfo',
            'src'       => $theme_url . '/assets/css/blocks/innerpage/product-info.css',
            'enqueue'   => is_product()
        ]
    );

    // wishlists Styles
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-mylists',
            'src'       => $theme_url . '/assets/css/blocks/innerpage/page-wishlists.css',
            'enqueue'   => ( is_page('my-lists') || $wl_parent )
        ]
    );

    // box-button Styles
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-boxbutton',
            'src'       => $theme_url . '/assets/css/blocks/homepage/box-button.css',
            'enqueue'   => ( is_woocommerce() || is_page('my-lists') || $wl_parent  )
        ]
    );

    // FX Woocommerce Styles
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-woocommerce',
            'src'       => $theme_url . '/assets/css/fx-woocommerce.css',
            'enqueue'   => ( is_woocommerce() || is_page('my-lists') || $wl_parent || is_cart() || is_checkout() || is_account_page() )
        ]
    );

    // FX Accordion Block Styles
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-accordion-block',
            'src'       => $theme_url . '/assets/css/blocks/innerpage/accordion.css',
            'enqueue'   => is_woocommerce()
        ]
    );

    // FX CTA Form Shop Block Styles
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-cta-form-block',
            'src'       => $theme_url . '/assets/css/blocks/innerpage/cta-form.css',
            'enqueue'   => is_woocommerce()
        ]
    );

    //  Page Category Styles
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-page-category',
            'src'       => $theme_url . '/assets/css/blocks/innerpage/page-category.css',
            'enqueue'   => is_woocommerce()
        ]
    );

    //  Up-sells Styles
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-upsells',
            'src'       => $theme_url . '/assets/css/blocks/homepage/product-category.css',
            'enqueue'   => is_product()
        ]
    );

    // Yith Compare Styles
    fx_assets_add_stylesheet(
        [
            '' => 'fx-compare-page',
            'src' => $theme_url . '/assets/css/blocks/innerpage/page-compare.css',
            'enqueue' => ( is_page('yith-compare') || is_shop() || is_product() )
        ]
    );

    /* Component-specific css. These will be enqueued per-block as dependencies or per-page as needed. These files can be edited to override default styling. */

    // Styles for social sharing buttons on blog pages
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx_social',
            'src'       => $theme_url . '/assets/css/components/FxSocialShare.css',
            'enqueue'   => ( !is_product() && is_single() )
        ]
    );

    // Styles specifically for CF7 forms.
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx_cf7',
            'src'       => $theme_url . '/assets/css/components/cf7.css',
        ]
    );

    // Styles for accordion block sections
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx_accordion',
            'src'       => $theme_url . '/assets/css/components/FxAccordion.css',
        ]
    );

    // Styles for tab/accordion block sections
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx_tabs_accordion',
            'src'       => $theme_url . '/assets/css/components/FxTabsAccordion.css',
            'enqueue'   => is_search()
        ]
    );

    // Styles for tab/accordion block sections
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx_tabs_accordion',
            'src'       => $theme_url . '/assets/css/components/FxTabsAccordion.css',
            'enqueue'   => is_search()
        ]
    );

    // Page Category Scripts
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx-woo-cat-accordion',
            'src'       => $theme_url . '/assets/css/blocks/innerpage/cat-accordion.css',
            'enqueue'   => is_product_category()
        ]
    );

    // Custom styling for choices.js library
    fx_assets_add_stylesheet(
        [
            'handle'        => 'fx_choices_custom',
            'src'           => $theme_url . '/assets/css/components/choices.css',
            'dependencies'  => [ 'fx_choices_plugin' ],
            'enqueue'       => ( !is_admin() || $wl_parent || is_archive() || is_home() || is_page() )
        ]
    );

    // Custom styling for slick library
    fx_assets_add_stylesheet(
        [
            'handle'        => 'fx_slick',
            'src'           => $theme_url . '/assets/css/components/slick.css',
            'dependencies'  => ['fx_slick_plugin'],
        ]
    );

    // Custom styling for ninja tables. TODO: remove if not using ninja tables.
    fx_assets_add_stylesheet(
        [
            'handle'        => 'fx_ninja',
            'src'           => $theme_url . '/assets/css/components/ninja-tables.css',
            'dependencies'  => ['fx_ninja_plugin'],
        ]
    );

     // Shared styling for show/hide text sections. TODO: remove if not using Show/Hide content
     fx_assets_add_stylesheet(
        [
            'handle'        => 'fx_hide_show',
            'src'           => $theme_url . '/assets/css/components/hide-show.css',
        ]
    );

    /* Plugin-specific css dependencies. These will be enqueued per-block as dependencies or per-page as needed. These files should not be edited directly. */

    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx_fancybox',
            'src'       => $theme_url . '/assets/css/plugins/fancybox.css',
        ]
    );

    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx_slick_plugin',
            'src'       => $theme_url . '/assets/css/plugins/slick.css',
        ]
    );

    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx_choices_plugin',
            'src'       => $theme_url . '/assets/css/plugins/choices.css',
        ]
    );

    // TODO: remove if not using ninja tables.
    fx_assets_add_stylesheet(
        [
            'handle'    => 'fx_ninja_plugin',
            'src'       => $theme_url . '/assets/css/plugins/ninja-tables.css',
        ]
    );

    // custom dependencies for WP plugins. Ensures that assigned stylesheets loads BEFORE plugin's stylesheets(s) load
    fx_assets_add_plugin_style( 'contact-form-7', 'fx_choices_custom' );
    fx_assets_add_plugin_style( 'contact-form-7', 'fx_cf7' );

    // TODO: remove below if not using ninja tables.
    fx_assets_add_plugin_style( 'footable_styles', 'fx_ninja' );
}


/**
 * Register and enqueue trump styles
 *
 * @hooked wp_enqueue_scripts priority 103 so block asset manager completes running and trump styles are enqueued last.
 */
add_action( 'wp_enqueue_scripts', 'fx_theme_style_trumps', 9999 );
function fx_theme_style_trumps() {
    fx_assets_add_stylesheet(
        [
            'handle'        => 'site-trumps',
            'src'           => get_template_directory_uri() . '/assets/css/trumps.css',
            'enqueue'       => !is_admin()
        ]
    );
}

/**
 * Register and enqueue theme scripts
 *
 * @return void
 */
add_action( 'wp_enqueue_scripts', 'fx_theme_scripts' );
function fx_theme_scripts() {
    $theme_dir = get_template_directory();
    $theme_url = get_template_directory_uri();

    $post = is_page() ? get_queried_object() : false;
    $wl_parent = "";

    if ( !empty($post) && is_a($post, 'WP_Post') ) {
        $wl_parent = (  93 === $post->post_parent );
    }

    // Scripts that must be included on every page.
    fx_assets_add_script(
        [
            'handle'        => 'site-global',
            'src'           => $theme_url . '/assets/js/global.js',
            'dependencies'  => [ 'jquery', 'fx_fitvids' ],
            'enqueue'       => ( !is_admin() || wp_is_mobile() ),
            'defer'         => true,
            'preload'       => true,
        ]
    );


    /**
     * Component-specific JS
     *
     * These will be enqueued per-block as dependencies or per-page as needed. These files can be edited to override
     * default behavior if necessary.
     */
    //

    // Range Filter Scripts
    fx_assets_add_script(
        [
            'handle'    => 'fx-product-range-filter',
            'src'       => $theme_url . '/assets/js/blocks/innerpage/range-slider.js',
            'dependencies'  => [ 'fx_foundation' ],
            'enqueue'   => is_woocommerce()
        ]
    );

    // Product Slick Slider Scripts
    fx_assets_add_script(
        [
            'handle'    => 'fx-product-slider',
            'src'       => $theme_url . '/assets/js/blocks/innerpage/product-slick-slider.js',
            'dependencies'  => [ 'fx_slick' ],
            'enqueue'   => is_product()
        ]
    );

    // Single Product Testimonial Section Styling
    fx_assets_add_script(
        [
            'handle'    => 'single-product-testimonial',
            'src'       => $theme_url . '/assets/js/blocks/homepage/testimonial-slider.js',
            'dependencies'  => [ 'fx_slick' ],
            'enqueue'   => is_product()
         ]
    );

    fx_assets_add_script(
        [
            'handle'    => 'single-product-testimonial',
            'src'       => $theme_url . '/assets/js/blocks/homepage/testimonial-slider.js',
            'dependencies'  => [ 'fx_slick' ],
            'enqueue'   => is_product()
         ]
    );

    fx_assets_add_script(
        [
            'handle'    => 'homepage-case-finder',
            'src'       => $theme_url . '/assets/js/blocks/homepage/case-finder.js',
            'dependencies'  => [ 'jquery' ],
            'enqueue'   => is_home()
         ]
    );

    fx_assets_add_script(
        [
            'handle'        => 'fx_choices',
            'src'           => $theme_url . '/assets/js/components/FxChoices.js',
            'dependencies'  => [ 'fx_choices_plugin' ],
            'defer'         => true,
            'enqueue'       => ( is_woocommerce() || is_page('my-lists') || $wl_parent || is_archive() || is_home() || is_page() )
        ]
    );

    // Page Category Scripts
    fx_assets_add_script(
        [
            'handle'    => 'fx-page-category',
            'src'       => $theme_url . '/assets/js/blocks/innerpage/category-accordion.js',
            'enqueue'   => is_product_category()
        ]
    );

    // Accordion Block Scripts
    fx_assets_add_script(
        [
            'handle'    => 'fx-accordion-block',
            'src'       => $theme_url . '/assets/js/blocks/innerpage/accordion.js',
            'enqueue'   => is_woocommerce()
        ]
    );

    // Script for block sections that use parallax
    fx_assets_add_script(
        [
            'handle'        => 'fx_parallax',
            'src'           => $theme_url . '/assets/js/components/FxParallax.js',
            'dependencies'  => [ 'jquery' ],
            'defer'         => true,
        ]
    );

    // Script social sharing buttons on blog pages
    fx_assets_add_script(
        [
            'handle'        => 'fx_social',
            'src'           => $theme_url . '/assets/js/components/FxSocialShare.js',
            'enqueue'       => is_single()
        ]
    );

    // Script for "Read More" block sections
    fx_assets_add_script(
        [
            'handle'        => 'fx_readmore',
            'src'           => $theme_url . '/assets/js/components/FxReadMore.js',
            'dependencies'  => [ 'jquery' ],
        ]
    );

    // Script for accordion block sections
    fx_assets_add_script(
        [
            'handle'        => 'fx_accordion',
            'src'           => $theme_url . '/assets/js/components/FxAccordion.js',
        ]
    );

    // Script for tabs/accordion block sections
    fx_assets_add_script(
        [
            'handle'        => 'fx_tabs_accordion',
            'src'           => $theme_url . '/assets/js/components/FxTabsAccordion.js',
            'enqueue'       => is_search()
        ]
    );

    // Script for block sections that use parallax
    fx_assets_add_script(
        [
            'handle'        => 'fx_parallax',
            'src'           => $theme_url . '/assets/js/components/FxParallax.js',
            'dependencies'  => [ 'jquery' ],
            'defer'         => true,
        ]
    );

    // Script for hide/show text content
    fx_assets_add_script(
        [
            'handle'        => 'fx_hide_show',
            'src'           => $theme_url . '/assets/js/components/hide-show.js',
        ]
    );


    /**
     * Plugin-specific JS
     *
     * Enqueued per-block as dependencies or per-page as needed. These files should not be edited directly.
     */
    fx_assets_add_script(
        [
            'handle'        => 'fx_slick',
            'src'           => $theme_url . '/assets/js/plugins/slick.js',
            'dependencies'  => [ 'jquery' ],
        ]
    );

    // fx_assets_add_script(
    //     [
    //         'handle'        => 'fx_choices_plugin',
    //         'src'           => $theme_url . '/assets/js/plugins/choices.js',
    //         'minify'        => false,
    //     ]
    // );

    fx_assets_add_script(
        [
            'handle'        => 'fx_fitvids',
            'src'           => $theme_url . '/assets/js/plugins/fitvids.js',
            'dependencies'  => [ 'jquery' ],
            'defer'         => true,
            'enqueue'       => true
        ]
    );

    fx_assets_add_script(
        [
            'handle'        => 'fx_fancybox',
            'src'           => $theme_url . '/assets/js/plugins/fancybox.js',
            'dependencies'  => [ 'jquery' ],
            'defer'         => true,
        ]
    );

    fx_assets_add_script(
        [
            'handle'        => 'fx_foundation',
            'src'           => $theme_url . '/assets/js/plugins/foundation.min.js',
            'dependencies'  => [ 'jquery' ],
            'defer'         => true,
        ]
    );

    fx_assets_add_script(
        [
            'handle'        => 'fx_phone_formatter',
            'src'           => $theme_url . '/assets/js/plugins/FormatPhoneNumbers.js',
            'dependencies'  => [ 'jquery' ],
            'defer'         => true,
        ]
    );

    // custom dependencies for WP plugins. Ensures that assigned scripts loads BEFORE plugin's script(s) load
    fx_assets_add_plugin_script( 'contact-form-7', 'fx_choices' );
    fx_assets_add_plugin_script( 'contact-form-7', 'fx_phone_formatter' );

    if( !is_admin() ) {
        wp_localize_script(
            'site-global',
            'FX',
            [
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'siteurl' => site_url(),
            ]
        );
    }
}


/**
 * Prevents CF7 scripts from being inlined if CF7 is in first block on page
 *
 * @param   array   $excluded_blocks    Excluded blocks
 * @return  array   Excluded blocks
 */
add_filter( 'fx_bam_do_not_inline_block_assets', 'fx_exclude_cf7_asset_inline' );
function fx_exclude_cf7_asset_inline( array $excluded_blocks ): array {
    $excluded_blocks[] = 'acf/cf7-block';

    return $excluded_blocks;
}


/**
 * Remove jQuery Migrate as a dependency for jQuery to improve load times
 *
 * @param   WP_Scripts  $scripts    WP_Scripts object
 * @return  void
 */
add_action( 'wp_default_scripts', 'fx_dequeue_jquery_migrate' );
function fx_dequeue_jquery_migrate( WP_Scripts $scripts ): void {
    if( !is_admin() && !empty( $scripts->registered['jquery'] ) ) {
        $scripts->registered['jquery']->deps = array_diff(
            $scripts->registered['jquery']->deps,
            [ 'jquery-migrate' ]
        );
    }
}
