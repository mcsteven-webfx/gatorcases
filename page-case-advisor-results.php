<?php get_header(); ?>

<?php get_template_part('partials/masthead'); ?>

<div class="container">

<div class="nm-row return_results">
    <div class="col-xs-12">
        <?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>

        <?php
        $instrument_dims = isset($_GET['instrumentdims']) ? $_GET['instrumentdims'] : '';
        $man             = isset($_GET['man']) ? $_GET['man'] : '';
        $product         = isset($_GET['product_name']) ? $_GET['product_name'] : '';
        $search_type     = isset($_GET['type']) ? $_GET['type'] : '';

        $measurement_type         = isset($_GET['measurement']) ? $_GET['measurement'] : 'Inches';
        $measurement_type_convert = $measurement_type === 'Millimeters' ? 0.0393701 : 1;
        $length                   = isset($_GET['length']) ? bcmul_alternative( (float)$_GET['length'], $measurement_type_convert ) : 0;
        $width                    = isset($_GET['width']) ? bcmul_alternative( (float)$_GET['width'], $measurement_type_convert) : 0;
        $height                   = isset($_GET['height']) ? bcmul_alternative( (float)$_GET['height'], $measurement_type_convert) : 0;
        $measurement_array        = [$length, $width, $height ];
        $instrument               = isset($_GET['instrument']) ? $_GET['instrument'] : '';
        $search_string            = $man && $product ? $man . " " . $product : $instrument;

        // search for non-guitar products
        if($search_type == "Other"){

            sort($measurement_array, SORT_NUMERIC);

            $min = $measurement_array[0];
            $mid = $measurement_array[1];
            $max = $measurement_array[2];

            $display_length =  $_GET['length'];
            $display_width =  $_GET['width'];
            $display_height =  $_GET['height'];

            echo "<div><h3>Showing results for $search_string</h3> Length: $display_length Width: $display_width Height: $display_height in $measurement_type</div>";

            $results = fx_get_other_case_finder_results($min, $mid, $max, $instrument_dims);
            $return_results = [];
            $measurement_store = 99999999999;
            foreach($results as $result){

                $measurement = $result['Int_Length'] * $result['Int_Width'] * $result['Int_Height'];
                $measurement = $measurement < $measurement_store ? $measurement : $measurement_store;
                $measurement_store = $measurement;

                $result_strip = $result['Model_Lookup'];
                array_push($return_results, $result_strip);
            }

            if(count($return_results)){
                $fit_kings = [];
                $regular_fit = "";
                $other_fit = "";
                $args = array(
                        'post_type'         => 'product',
                        'post_status'       => 'publish',
                        'posts_per_page'    => 10,
                        'meta_key'          => 'model',
                        'meta_value'        => $return_results,
                );
                // The Query
                $query = new WP_Query( $args );

                // The Loop
                if ( $query->have_posts() ) {
                    while ( $query->have_posts() ) :
                        $query->the_post();
                        $post_int_width = get_field('interior_width');
                        $post_int_length = get_field('interior_length');
                        $post_int_height = get_field('interior_height');
                        $post_measurement_product = (float)$post_int_width * (float)$post_int_length * (float)$post_int_height;
                        $img_url = get_the_post_thumbnail_url();

                        if (!$img_url) {
                            $img_url = '/wp-content/uploads/2022/01/semi-hollow-watermark.jpg';
                        }

                        if($post_measurement_product == $measurement_store){
                            array_push($fit_kings, $post);
                        } else {
                            $regular_fit .='<div class="col-md-6 cart-container">
                            <div class="cart-feature-image">
                                <img src="'.$img_url.'" />
                            </div>
                            <div class="cart-feature-content">
                                <div class="cart-post-content">
                                    <h3>'.fx_get_product_name(get_the_title(), get_field('model')).'</h3>
                                    <p>'.get_field('model').'</p>
                                    <p>'.get_the_content().'
                                    </p>
                                    <div class="btn-wrap">
                                        <a href="'.get_the_permalink().'" class="btn btn-primary">View</a>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>';

                        }
                    endwhile;
                }

                // Restore original Post Data
                wp_reset_postdata();

                $king_fit = '<h3 class="might_fit">Best Fit</h3><p class="might_fit_p">Based on the dimensions of your product, this solution is the most optimal fit. The results below are also excellent matches.</p>';
                foreach ($fit_kings as $p) {
                    global $post;
                    $post = $p;
                    setup_postdata($p);

                    $img_url = get_the_post_thumbnail_url();
                    if (!$img_url) {
                        $img_url = '/wp-content/uploads/2022/01/semi-hollow-watermark.jpg';
                    }

                    $king_fit .= '<div class="col-md-6 cart-container">
                        <div class="cart-feature-image">
                            <img src="'.$img_url.'" />
                        </div>
                        <div class="cart-feature-content">
                            <div class="cart-post-content">
                                <h3>'.fx_get_product_name(get_the_title(), get_field('model')).'</h3>
                                <p>'.get_field('model').'</p>
                                <p>'.get_the_content().'</p>
                                <div class="btn-wrap">
                                    <a href="'.get_the_permalink().'" class="btn btn-primary">View</a>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div><div class="col-md-12"><hr></div>';
                }

                wp_reset_postdata();

                if ( count($fit_kings) > 0) {
                    echo $king_fit;
                }

                echo $regular_fit;
                echo '<br>';

                if(isset($return_results_other[0]) && $return_results_other[0] == 'USA Custom Mixer Road Cases' && $king_fit == "" && $regular_fit == "") {
                    echo "<div class='no_results'>Looks like we couldn't find a fit. <a href='javascript:void(0)' class='lightbox-popup'>Try your search again.</a></div> <!--<div class='no_results'>Looks like we couldn't find a fit. <a href='javascript:void(0)' class='lightbox-popup'>Try your search again</a> or <a href='/custom-cases/'>contact us</a> for a customized solution.</div>-->";
                }
            }
            else {
                echo "<div class='no_results'>Looks like we couldn't find a fit. <a href='javascript:void(0)' class='lightbox-popup'>Try your search again.</a></div>";
            }
        } // End "other" product search
        else { // search for guitars
            $body_length = isset($_GET['body-length']) ? bcmul_alternative( (float)$_GET['body-length'], $measurement_type_convert ) : 0;
            $body_height = isset($_GET['body-height']) ? bcmul_alternative( (float)$_GET['body-height'], $measurement_type_convert) : 0;
            $lower_bout = isset($_GET['lower-bout']) ? bcmul_alternative( (float)$_GET['lower-bout'], $measurement_type_convert) : 0;
            $middle_bout = isset($_GET['middle-bout']) ? bcmul_alternative( (float)$_GET['middle-bout'], $measurement_type_convert) : 0;
            $upper_bout = isset($_GET['upper-bout']) ? bcmul_alternative( (float)$_GET['upper-bout'], $measurement_type_convert) : 0;
            $overall_length = isset($_GET['overall-length']) ? bcmul_alternative( (float)$_GET['overall-length'], $measurement_type_convert) : 0;

            $return_results = [];
            if($instrument == "Other") {
                // Ranges to search by
                $return_results = fx_get_guitar_case_finder_results([
                    'body_length_min'    => $body_length - 0.1,
                    'body_length_max'    => $body_length + 4,
                    'body_height_min'    => $body_height,
                    'body_height_max'    => $body_height + 2,
                    'lower_bout_min'     => $lower_bout,
                    'lower_bout_max'     => $lower_bout + 1.5,
                    'middle_bout_min'    => $middle_bout,
                    'middle_bout_max'    => $middle_bout + 2,
                    'upper_bout_min'     => $upper_bout,
                    'upper_bout_max'     => $upper_bout + 2,
                    'overall_length_min' => $overall_length,
                    'overall_length_max' => $overall_length + 4,
                ]);
            }
            else {
                // Ranges to search by
                $return_results = fx_get_guitar_case_finder_results([
                    'body_length_min'    => $body_length - 0.3,
                    'body_length_max'    => $body_length + 6,
                    'body_height_min'    => $body_height,
                    'body_height_max'    => $body_height + 2.5,
                    'lower_bout_min'     => $lower_bout - 0.1,
                    'lower_bout_max'     => $lower_bout + 1.75,
                    'middle_bout_min'    => $middle_bout - 0.1,
                    'middle_bout_max'    => $middle_bout + 5,
                    'upper_bout_min'     => $upper_bout - 0.1,
                    'upper_bout_max'     => $upper_bout + 5,
                    'overall_length_min' => $overall_length - 0.1,
                    'overall_length_max' => $overall_length + 6,
                    'instrument'         => $instrument,
                ]);
            }

            if(count($return_results)) :
                $fit_kings = [];
                $other_fit = "";
                $args = array(
                        'post_type'         => 'product',
                        'post_status'       => 'publish',
                        'posts_per_page'    => -1,
                        'meta_key'          => 'model',
                        'meta_value'        => $return_results,
                );
                // The Query
                $query = new WP_Query( $args );

                // The Loop
                if ( $query->have_posts() ) {
                    // Show user what they are searching
                    $display_length =  $_GET['body-length'];
                    $display_height =  $_GET['body-height'];
                    $display_lower =  $_GET['lower-bout'];
                    $display_middle =  $_GET['middle-bout'];
                    $display_upper =  $_GET['upper-bout'];
                    $display_overall =  $_GET['overall-length'];
                    ?>

                    <div>
                        <h3>Showing results for <?php echo $search_string; ?></h3>
                        <p>Body Length: <?php echo $display_length; ?> Body Height: <?php echo $display_height; ?> Lower Bout: <?php echo $display_lower; ?> Middle Bout: <?php echo $display_middle; ?> Upper Bout: <?php echo $display_upper; ?> Overall Length: <?php echo $display_overall; ?> in <?php echo $measurement_type; ?></p>
                    </div>

                    <?php while ( $query->have_posts() ) :
                        $query->the_post();
                        $img_url = get_the_post_thumbnail_url();

                        if (!$img_url) {
                            $img_url = '/wp-content/uploads/2022/01/semi-hollow-watermark.jpg';
                        }
                        ?>

                        <div class="col-md-6 cart-container">
                            <div class="cart-feature-image">
                                <img src="<?php echo $img_url; ?>" />
                            </div>
                            <div class="cart-feature-content">
                                <div class="cart-post-content">
                                    <h3><?php echo fx_get_product_name(get_the_title(), get_field('model')); ?></h3>
                                    <p><?php the_field('model'); ?></p>
                                    <p><?php the_content(); ?></p>
                                    <div class="btn-wrap">
                                        <a href="<?php the_permalink(); ?>" class="btn btn-primary">View</a>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                    <?php endwhile;
                }

                // Restore original Post Data
                wp_reset_postdata();

            else : ?>
                <div class='no_results'>Looks like we couldn't find a fit. <a href='javascript:void(0)' class='lightbox-popup'>Try your search again.</a></div>;
            <?php endif;
        } // End guitar search
        ?>

        <?php endwhile;	?>

        <?php endif; ?>
    </div>
</div>

</div>

<?php get_footer();
