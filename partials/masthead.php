<?php
    $masthead = "";

    if ( is_woocommerce() ) {
        $masthead = 'masthead-product';
    } else {
        $masthead = 'masthead';
    }
?>
<section class="<?php echo $masthead;?>">
    <div class="container">
        <?php if ( is_search() ): ?>
            <h3 class="h1">Search Results</h3><?php /* different heading type for SEO benefit */ ?>
        <?php elseif ( is_home() ): ?>
            <?php
                if( function_exists( 'yoast_breadcrumb' ) ) {
                    yoast_breadcrumb( '<div class="breadcrumbs hidden-sm-down"><ul class="clearfix">', '</ul></div>' );
                }
            ?>
            <h3 class="h1">Blog</h3><?php /* different heading type for SEO benefit */ ?>
        <?php elseif ( is_404() ) : ?>
            <h1>404: Oops! We can't find the page you're looking for.</h1>

        <?php elseif ( is_page('yith-compare') ) : ?>
            <?php
                if( function_exists( 'yoast_breadcrumb' ) ) {
                    yoast_breadcrumb( '<div class="breadcrumbs hidden-sm-down"><ul class="clearfix">', '</ul></div>' );
                }
            ?>
        <?php else : ?>
            <?php
             $photo_header = get_field('innerpage_featured_image');
             if ( $photo_header === true ) {
            ?>
                <div class="masthead-content">
                    <div class="masthead-img">
                        <?php echo fx_get_image_tag( get_post_thumbnail_id() , '', 'full', false, get_the_title() ); ?>
                    </div>
                    <div class="masthead-text text-center">
                        <?php if ( get_field('page_subhead') ) : ?>
                            <div class="optional-heading"><?php echo get_field('page_subhead'); ?></div>
                        <?php endif; ?>
                        <h1 class="h2"><?php the_title(); ?></h1>
                    </div>
                </div>
                <?php
                    if( function_exists( 'yoast_breadcrumb' ) ) {
                        yoast_breadcrumb( '<div class="breadcrumbs hidden-sm-down"><ul class="clearfix">', '</ul></div>' );
                    }
                ?>
            <?php
                } else {
            ?>
            <h1 class="h2"><?php the_title(); ?></h1>
            <?php
                if( function_exists( 'yoast_breadcrumb' ) ) {
                    yoast_breadcrumb( '<div class="breadcrumbs hidden-sm-down"><ul class="clearfix">', '</ul></div>' );
                }
            ?>
        <?php
                } //end of else statement $photo_header 
            endif;
        ?>
    </div>
</section>
