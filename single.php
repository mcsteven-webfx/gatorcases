<?php get_header(); ?>
<?php get_template_part('partials/masthead'); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="container">
        <?php get_template_part( 'partials/social-share' ); ?>
    </div>
    <?php the_content(); ?>
<?php endwhile; endif; ?>

<?php get_footer();