<?php
/*
	Hooks for custom templates
*/
if ( ! function_exists( 'fx_woo_wishlists' ) ) {

    /**
     * Output the product wishlist link.
     */
    function fx_woo_wishlists() {
        wc_get_template( 'add-to-wishlist-link.php' );
    }
}

if ( ! function_exists( 'fx_woo_loop_thumbnail' ) ) {

    /**
     * Output the product image.
     */
    function fx_woo_loop_thumbnail() {
        wc_get_template( 'loop/loop-image.php' );
    }
}

if ( ! function_exists( 'fx_woo_loop_meta' ) ) {

    /**
     * Output the product meta.
     */
    function fx_woo_loop_meta() {
        wc_get_template( 'loop/loop-meta.php' );
    }
}

if ( ! function_exists( 'woocommerce_template_loop_product_title' ) ) {

	/**
	 * Show the product title in the product loop. By default this is an H2.
	 */
	function woocommerce_template_loop_product_title() {
        wc_get_template( 'loop/loop-product-title.php' );
	}
}

if ( ! function_exists( 'fx_loop_dealers_link' ) ) {

	/**
	 * Show the product dealers link
	 */
	function fx_loop_dealers_link() {
		if ( get_field('online_dealers') ) {
			echo '<a class="btn btn-secondary btn-block text-center" href="'.get_field('online_dealers').'">Shop Online Dealers</a>';
		}
	}
}

if ( ! function_exists( 'fx_loop_wrapper_start' ) ) {

	/**
	 * Show the product loop start
	 */
	function fx_loop_wrapper_start() {
		$cat_listing_wrap = "";
		if ( !is_shop() ) {
			$cat_listing_wrap = 'category-listings';
		} else {
			$cat_listing_wrap = 'category-listings-shop';
		}
		echo '<div class="'.$cat_listing_wrap.'">';
	}
}


if ( ! function_exists( 'fx_loop_wrapper_close' ) ) {

	/**
	 * Show the product loop close
	 */
	function fx_loop_wrapper_close() {

		echo '</div>';
	}
}


if ( ! function_exists( 'fx_woocommerce_product_archive_description' ) ) {

	/**
	 * Show a shop page description on product archives.
	 */
	function fx_woocommerce_product_archive_description() {
		// Don't display the description on search results page.
		if ( is_search() ) {
			return;
		}

		if ( is_post_type_archive( 'product' ) && in_array( absint( get_query_var( 'paged' ) ), array( 0, 1 ), true ) ) {
			$shop_page_id = get_option( 'woocommerce_shop_page_id' );

			echo apply_filters( 'the_content', get_post_field( 'post_content', absint($shop_page_id) ) );
		}
	}
}

if ( ! function_exists( 'fx_woo_dealers_link' ) ) {

	/**
	 * Show a shop page description on product archives.
	 */
	function fx_woo_dealers_link() {
		wc_get_template( 'single-product/dealers-link.php' );
	}
}

if ( ! function_exists( 'fx_woo_social_media_share' ) ) {

	/**
	 * Show a shop page description on product archives.
	 */
	function fx_woo_social_media_share() {
		wc_get_template( 'single-product/socialmedia-share.php' );
	}
}

if ( ! function_exists( 'fx_woo_single_product_print' ) ) {

	/**
	 * Show a shop page description on product archives.
	 */
	function fx_woo_single_product_print() {
		wc_get_template( 'single-product/print.php' );
	}
}

if ( ! function_exists( 'fx_woo_product_details' ) ) {

	/**
	 * Show a shop page description on product archives.
	 */
	function fx_woo_product_details() {
		wc_get_template( 'single-product/product-details-section.php' );
	}
}

if ( ! function_exists( 'fx_woo_product_testimonials' ) ) {

	/**
	 * Show a shop page description on product archives.
	 */
	function fx_woo_product_testimonials() {
		wc_get_template( 'single-product/testimonials.php' );
	}
}

if ( ! function_exists( 'fx_wc_output_all_notices' ) ) {

	/**
	 * Show's woocommerce notices
	 */
	function fx_wc_output_all_notices() {
		echo '<div class="woocommerce-notices-wrapper"><div class="container">';
		wc_print_notices();
		echo '</div></div>';
	}
}

if ( ! function_exists( 'woocommerce_subcategory_thumbnail' ) ) {

	/**
	 * Show subcategory thumbnails.
	 *
	 * @param mixed $category Category.
	 */
	function woocommerce_subcategory_thumbnail( $category ) {
		$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'woocommerce_thumbnail' );
		$dimensions           = wc_get_image_size( $small_thumbnail_size );
		$thumbnail_id         = get_term_meta( $category->term_id, 'thumbnail_id', true );

		if ( $thumbnail_id ) {
			$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
			$image        = $image[0];
			$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
			$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
		} else {
			$image        = wc_placeholder_img_src();
			$image_srcset = false;
			$image_sizes  = false;
		}

		if ( $image ) {
			// Prevent esc_url from breaking spaces in urls for image embeds.
			// Ref: https://core.trac.wordpress.org/ticket/23605.
			$image = str_replace( ' ', '%20', $image );

			// Add responsive image markup if available.
			if ( $image_srcset && $image_sizes ) {
				echo '<span class="cat-image"><img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" width="' . esc_attr( $dimensions['width'] ) . '" height="' . esc_attr( $dimensions['height'] ) . '" srcset="' . esc_attr( $image_srcset ) . '" sizes="' . esc_attr( $image_sizes ) . '" /></span>';
			} else {
				echo '<span class="cat-image"><img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" width="' . esc_attr( $dimensions['width'] ) . '" height="' . esc_attr( $dimensions['height'] ) . '" /></span>';
			}
		}
	}
}

if ( ! function_exists( 'woocommerce_template_loop_category_title' ) ) {

	/**
	 * Show the subcategory title in the product loop.
	 *
	 * @param object $category Category object.
	 */
	function woocommerce_template_loop_category_title( $category ) {
		?>
		<h2 class="woocommerce-loop-category__title">
			<?php
			echo esc_html( $category->name );

			if ( $category->count > 0 ) {
				// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				echo apply_filters( 'woocommerce_subcategory_count_html','', $category );
			}
			?>
		</h2>
		<?php
	}
}

if ( ! function_exists( 'fx_product_short_description' ) ) {

	/**
	 * Show the subcategory title in the product loop.
	 *
	 * @param object $category Category object.
	 */
	function fx_product_short_description( $category ) {
		?>
            <div class="product-short-description">
		        <?php the_field('short_description'); ?>
            </div>
		<?php
	}
}
