<?php
/**
 * Sidebar
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/sidebar.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$filters = array(
	array(
		'id' => 'product_type',
		'name' => 'Product Type'
	),
	array(
		'id' => 'price',
		'name' => 'Price'
	),
	array(
		'id' => 'length',
		'name' => 'Length'
	),
	array(
		'id' => 'width',
		'name' => 'Width'
	),
	array(
		'id' => 'height',
		'name' => 'Height'
	),
	array(
		'id' => 'drum_size',
		'name' => 'Drum Size'
	),
	array(
		'id' => 'drum_type',
		'name' => 'Drum Type'
	),
	array(
		'id' => 'equipment',
		'name' => 'Equipment'
	),
	array(
		'id' => 'style',
		'name' => 'Style'
	),
	array(
		'id' => 'guitar_type',
		'name' => 'Guitar Type'
	),
	array(
		'id' => 'instrument',
		'name' => 'Instrument'
	),
	array(
		'id' => 'stand_features',
		'name' => 'Stand Features'
	),
	array(
		'id' => 'keyboard_size',
		'name' => 'Keyboard Size'
	),
	array(
		'id' => 'features',
		'name' => 'Features'
	),
	array(
		'id' => 'rack_type',
		'name' => 'Rack Type'
	),
	array(
		'id' => 'section',
		'name' => 'Section'
	),
	array(
		'id' => 'material',
		'name' => 'Material'
	),
	array(
		'id' => 'other_filters',
		'name' => 'Other Filters'
	)
);

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
?>
<div class="category-sidebar">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="h2"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>
	<div class="filter-text">Filters</div>
	<div class="accordion">
		<div class="accordion-list">

            <?php foreach ($filters as $filter) : ?>
				<div class="cat-list-item">
					<h5 class="cat-title"><?php echo $filter['name']; ?></h5>
					<div class="cat-content">
		                <?php echo do_shortcode('[facetwp facet="' . $filter['id'] . '"]'); ?>
					</div>
				</div>
            <?php endforeach; ?>

            <button onclick="FWP.reset()">Reset Filters</button>
		</div>
	</div>
</div>
<?php
