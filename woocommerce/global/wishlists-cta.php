<?php if( have_rows( 'wishlists_cta', 93) ): ?>
    <div class="wishlist-sidebar">
        <div class="row">
            <?php while( have_rows( 'wishlists_cta', 93) ) : the_row(); ?>
                <div class="col-lg-12">
                    <div class="box-content">
                        <div class="box-img clearfix">
                            <?php echo fx_get_image_tag( get_sub_field( 'image' ), '', 'full', get_sub_field('headline') ); ?>
                        </div>
                        <div class="box-text">
                            <h3><?php echo get_sub_field('head');?></h3>
                            <?php if ( get_sub_field('subhead') ) : ?>
                                <p><?php echo get_sub_field('subhead');?></p>
                            <?php endif; ?>
                            <?php if( $button = get_sub_field( 'link' )  ): ?>
                                <a
                                    class="btn btn-primary"
                                    href="<?php echo esc_url( $button['url'] ); ?>"
                                    target="<?Php echo $button['target'] ? $button['target'] : '_self';?>"
                                >
                                    <?php echo $button['title']; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>