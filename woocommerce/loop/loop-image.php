<?php
    if ( ! defined( 'ABSPATH' ) ) {
        exit;
    }
    global $product;
?>
<div class="<?php echo is_product() ? 'product-box' : 'category-box'; ?>">
    <div class="<?php echo is_product() ? 'product-img-box' : 'category-img-box'; ?>">
        <a href="#" class="icon-whislist-link"><i class="icon-whistlist red-icon"></i></a>
        <div class="<?php echo is_product() ? 'product-img' : 'category-img'; ?>">
            <a href="<?php echo get_permalink( $product->get_id() ); ?>">
                <?php echo woocommerce_get_product_thumbnail();?>
            </a>
            <div class="<?php echo is_product() ? 'product-box-bttn' : 'category-box-bttn'; ?> text-center">
                <a href="<?php echo get_permalink( $product->get_id() ); ?>" class="btn btn-secondary">View Details</a>
            </div>
        </div>
    </div>
</div>
