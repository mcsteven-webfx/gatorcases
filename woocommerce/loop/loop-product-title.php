<?php
    if ( ! defined( 'ABSPATH' ) ) {
        exit;
    }
    global $product;
?>
<a class="product-loop-title" href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
<?php