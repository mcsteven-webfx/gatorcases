<?php do_action( 'woocommerce_wishlists_before_wrapper' ); ?>
    <?php $lists = WC_Wishlists_User::get_wishlists(); ?>
    <?php if ( function_exists( 'wc_print_messages' ) ) : ?>
        <?php wc_print_messages(); ?>
    <?php else : ?>
        <?php WC_Wishlist_Compatibility::wc_print_notices(); ?>
    <?php endif; ?>

    <?php $max_list_count = apply_filters( 'wc_wishlists_max_user_list_count', '*' ); ?>

<section class="page-wishlist">
    <div class="wishlist-wrapper">
        <div class="wishlist-information">
            <div class="wishlist-top-info">
                <?php if ( $max_list_count === '*' || ( empty( $lists ) || count( $lists ) < $max_list_count ) ): ?>
                    <a class="btn btn-secondary" href="<?php echo WC_Wishlists_Pages::get_url_for( 'create-a-list' ); ?>">
                        <i class="icon-plus"></i> <?php _e( 'Create a New List', 'wc_wishlist' ); ?>
                    </a>
                <?php endif; ?>
            </div>
            <div class="wishlist-table">
                <?php if ( $lists && count( $lists ) ) : ?>
                    <form method="post">
                        <div class="table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?php _e( 'List Name', 'wc_wishlist' ); ?></th>
                                        <th><?php _e( 'Date Added', 'wc_wishlist' ); ?></th>
                                        <th><?php _e( 'Privacy Settings', 'wc_wishlist' ); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ( $lists as $list ) : ?>
                                        <?php
                                            $sharing = $list->get_wishlist_sharing();
                                        ?>
                                        <tr>
                                            <td class="wl-name">
                                                <h5><a href="<?php $list->the_url_edit(); ?>"><?php $list->the_title(); ?></a></h5>
                                                <div class="row-actions">
                                                        <span class="edit">
                                                            <small><a rel="nofollow" href="<?php $list->the_url_edit(); ?>"><?php _e( 'Manage this list', 'wc_wishlist' ); ?></a></small>
                                                        </span>
                                                    |
                                                    <span class="trash">
                                                            <small><a rel="nofollow" class="ico-delete wlconfirm" data-message="<?php _e( 'Are you sure you want to delete this list?', 'wc_wishlist' ); ?>" href="<?php $list->the_url_delete(); ?>"><?php _e( 'Delete', 'wc_wishlist' ); ?></a></small>
                                                        </span>
                                                    <?php if ( $sharing == 'Public' || $sharing == 'Shared' ) : ?>
                                                        |
                                                        <span class="view">
                                                                <small><a rel="nofollow" href="<?php $list->the_url_view(); ?>&preview=true"><?php _e( 'Preview', 'wc_wishlist' ); ?></a></small>
                                                            </span>
                                                    <?php endif; ?>
                                                </div>
                                            </td>
                                            <td><?php echo date_i18n( get_option( 'date_format' ), strtotime( $list->post->post_date ) ); ?></td>
                                            <td>
                                                <div class="wishlist-select">
                                                    <select name="sharing[<?php echo $list->id; ?>]">
                                                        <option <?php selected( $sharing, 'Public' ); ?> value="Public"><?php _e( 'Public', 'wc_wishlist' ); ?></option>
                                                        <option <?php selected( $sharing, 'Shared' ); ?> value="Shared"><?php _e( 'Shared', 'wc_wishlist' ); ?></option>
                                                        <option <?php selected( $sharing, 'Private' ); ?> value="Private"><?php _e( 'Private', 'wc_wishlist' ); ?></option>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                        <td class="actions">
                                            <input type="submit" class="btn btn-primary" name="update_wishlists" value="<?php _e( 'Save Changes', 'wc_wishlist' ); ?>"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                <?php else : ?>
                    <?php $shop_url = get_permalink( WC_Wishlist_Compatibility::wc_get_page_id( 'shop' ) ); ?>
                    <?php _e( 'You have not created any lists yet.', 'wc_wishlist' ); ?>
                    <a href="<?php echo $shop_url; ?>"><?php _e( 'Go shopping to create one.', 'wc_wishlist' ); ?></a>
                <?php endif; ?>
            </div>
        </div>
        <?php if( have_rows( 'wishlists_cta' ) ): ?>
            <div class="wishlist-sidebar">
                <div class="row">
                    <?php while( have_rows( 'wishlists_cta' ) ) : the_row(); ?>
                        <div class="col-lg-12">
                            <div class="box-content">
                                <div class="box-img clearfix">
                                    <?php echo fx_get_image_tag( get_sub_field( 'image' ), '', 'full', get_sub_field('headline') ); ?>
                                </div>
                                <div class="box-text">
                                    <h3><?php echo get_sub_field('head');?></h3>
                                    <?php if ( get_sub_field('subhead') ) : ?>
                                        <p><?php echo get_sub_field('subhead');?></p>
                                    <?php endif; ?>
                                    <?php if( $button = get_sub_field( 'link' )  ): ?>
                                        <a
                                            class="btn btn-primary"
                                            href="<?php echo esc_url( $button['url'] ); ?>"
                                            target="<?Php echo $button['target'] ? $button['target'] : '_self';?>"
                                        >
                                            <?php echo $button['title']; ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
<?php do_action( 'woocommerce_wishlists_after_wrapper' ); ?>