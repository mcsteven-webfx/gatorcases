<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

do_action( 'woocommerce_before_customer_login_form' ); ?>



<section class="login-section">
	<div class="container">
		<h2><?php the_title(); ?></h2>
		<?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>
			<div class="row">
				<div class="col-md-6">
					<div class="login-item right-margin">
						<h3><?php esc_html_e( 'Login', 'woocommerce' ); ?></h3>

						<form class="login-box" method="post">

							<?php do_action( 'woocommerce_login_form_start' ); ?>
								<div class="form-field">
									<div class="form-field-floating">
										<input type="text" name="username" placeholder="Username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" />
										<label for="username"><?php esc_html_e( 'Username or email address', 'woocommerce' ); ?></label>
									</div>
								</div>
								<div class="form-field">
									<div class="form-field-floating">
										<input type="password" name="password" placeholder="Password" id="password" autocomplete="current-password" />
										<label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?></label>
									</div>
								</div>

							<?php do_action( 'woocommerce_login_form' ); ?>

							<div class="form-field">
								<div class="form-field-floating">
									<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
									<button type="submit" class="btn btn-block btn-primary text-center" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>">
										<?php esc_html_e( 'Log in', 'woocommerce' ); ?>
									</button>
									<p class="small">This site is protected by reCAPTCHA and the Google <a href="<?php echo get_permalink(3);?>">Privacy Policy</a> and <a href="<?php echo get_permalink(12);?>"">Terms of Service</a> apply.</p>
								</div>
							</div>
							<div class="check_box_area">
								<div>
									<input name="rememberme" type="checkbox" id="rememberme" value="forever" name="check-group" />
									<label for="remember-me">
										<?php esc_html_e( 'Remember me', 'woocommerce' ); ?>
									</label>
								</div>
							</div>
							<p>
								<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>">
									<?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?>
								</a>
							</p>
							<?php do_action( 'woocommerce_login_form_end' ); ?>

						</form>
					</div>
				</div>

				<div class="col-md-6">
					<div class="login-item left-margin">
						<h3><?php esc_html_e( 'Register', 'woocommerce' ); ?></h3>

						<form method="post" class="login-box" <?php do_action( 'woocommerce_register_form_tag' ); ?> >

							<?php do_action( 'woocommerce_register_form_start' ); ?>

							<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
								<div class="form-field">
									<div class="form-field-floating">
										<input type="text" name="username" id="reg_username" placeholder="Username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" />
										<label for="reg_username">
											<?php esc_html_e( 'Username', 'woocommerce' ); ?>
										</label>
									</div>
								</div>
							<?php endif; ?>

							<div class="form-field">
								<div class="form-field-floating">
									<input type="email" name="email" id="reg_email" placeholder="Email address" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
									<label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?></label>
								</div>
							</div>
							<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

								<div class="form-field">
									<div class="form-field-floating">
										<input type="password" name="password" id="reg_password"placeholder="Password"  autocomplete="new-password" />
										<label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?></label>
									</div>
								</div>
							<?php else : ?>

								<p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>

							<?php endif; ?>

							

							<div class="form-field">
								<div class="form-field-floating">
									<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
									<button type="submit" class="btn btn-block btn-primary text-center" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>">
										<?php esc_html_e( 'Register', 'woocommerce' ); ?>
									</button>
								</div>
								<?php do_action( 'woocommerce_register_form' ); ?>
							</div>

							

							<?php do_action( 'woocommerce_register_form_end' ); ?>

						</form>
					</div>			
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>


<?php do_action( 'woocommerce_after_customer_login_form' ); ?>