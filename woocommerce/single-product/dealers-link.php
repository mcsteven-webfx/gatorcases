<?php
/**
 * Dealers Link
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php if ( get_field( 'bar_code' ) ) : ?>
    <div class="col-sm-6">
        <a href="#obn" data-product="<?php the_field('bar_code'); ?>" class="btn btn-block btn-primary text-center buy-now">Online Dealers</a>
    </div>
<?php endif; ?>

<div class="col-sm-6">
    <a href="<?php echo get_field('local_dealers') ? get_field('local_dealers') : '#'; ?>" class="btn btn-block btn-primary text-center">Local Dealers</a>
</div>


<?php
