<?php
/**
 * Single Product Details Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

    global $product;
?>
<section class="accordion-section">
    <div class="row">
        <div class="col-md-12">
            <div class="accordion">
                <div class="accordion-list">
                    <div class="accordion-list-item">
                        <h5 class="accordion-title">Features</h5>
                        <div class="accordion-content">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-3">
                                    <?php wc_get_template_part( 'single-product/tabs/features' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-list-item">
                    <h5 class="accordion-title">Description</h5>
                    <div class="accordion-content">
                        <div class="row">
                            <div class="col-md-9 col-md-offset-3">
                                <?php echo get_the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-list-item">
                    <h5 class="accordion-title">What Fits This Case</h5>
                    <div class="accordion-content">
                        <div class="row">
                            <div class="col-md-9 col-md-offset-3">
                                <?php echo get_field('what_fit_case_description'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-list-item">
                    <h5 class="accordion-title">Product Specs</h5>
                    <div class="accordion-content">
                        <div class="row">
                            <div class="col-md-9 col-md-offset-3">
                                <?php wc_get_template_part( 'single-product/tabs/product-specs' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ( $video = get_field('video_link') ) : ?>
                    <div class="accordion-list-item">
                        <h5 class="accordion-title">Product Videos</h5>
                        <div class="accordion-content">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-3">
                                    <div class="product-video">
                                        <?php echo apply_filters('the_content', $video); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="accordion-list-item">
                    <h5 class="accordion-title">Downloads</h5>
                    <div class="accordion-content">
                        <div class="row">
                            <div class="col-md-9 col-md-offset-3">
                                <?php wc_get_template_part( 'single-product/tabs/downloads' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-list-item">
                    <h5 class="accordion-title">Warranty</h5>
                    <div class="accordion-content">
                        <div class="row">
                            <div class="col-md-9 col-md-offset-3">
                                <div class="">
                                    <?php echo get_field('warranty_text', 'option'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
