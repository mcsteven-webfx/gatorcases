<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters(
	'woocommerce_single_product_image_gallery_classes',
	array(
        'product-big'
	)
);
$attachment_ids = get_field('gallery');
?>
<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>">
    <div class="product-big-item woocommerce-product-gallery__image">
        <?php echo fx_get_image_tag( $post_thumbnail_id, 'zoomImg', 'full', false, get_the_title() ); ?>
    </div>
    <?php
        if ( $attachment_ids && $product->get_image_id() ) {
            while ( have_rows('gallery') ) { the_row();
    ?>
        <div class="product-big-item woocommerce-product-gallery__image">
            <?php echo  fx_get_image_tag( get_sub_field('gallery_image'), 'zoomImg', 'full', false, get_the_title() ); ?>
        </div>
    <?php
            }
        }
    ?>
</div>
<div class="product-thumb">
        <div class="product-thumb-item">
            <div class="product-border">
                <?php echo fx_get_image_tag( $post_thumbnail_id, '', 'full', false, get_the_title() ); ?>
            </div>
        </div>
    <?php
        if ( $attachment_ids && $product->get_image_id() ) {
            while ( have_rows('gallery') ) { the_row();
    ?>
        <div class="product-thumb-item">
            <div class="product-border">
                <?php echo  fx_get_image_tag( get_sub_field('gallery_image'), '', 'full', false, get_the_title() ); ?>
            </div>
        </div>
    <?php
            }
        }
    ?>
</div>
<?php
