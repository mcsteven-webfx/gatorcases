<?php
/**
 * Social Media Share
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/* Social share code generated from https://sharingbuttons.io/ */ 
?>
<ul>
    <li>SHARE:</li>
    <li class="social-share-row__item">
        <a class="resp-sharing-button__link js-social-share" href="mailto:?subject=<?php the_title();?>&amp;body=<?php echo esc_url( get_page_link() ); ?>" target="_self" rel="noopener" aria-label="">
            <i class="icon-mail"></i>
        </a>
    </li>
    <li class="social-share-row__item">
        <a class="resp-sharing-button__link js-social-share" href="https://twitter.com/intent/tweet/?text=<?php the_title();?>&amp;url=<?php echo esc_url( get_page_link() ); ?>" target="_blank" rel="noopener" aria-label="">
            <i class="icon-twitter"></i>
        </a>
    </li>
    
    <li class="social-share-row__item">
        <a class="resp-sharing-button__link js-social-share" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo esc_url( get_page_link() ); ?>&amp;title=<?php the_title();?>&amp;source=<?php echo esc_url( get_page_link() ); ?>" target="_blank" rel="noopener" aria-label="">
            <i class="icon-linkedin"></i>
        </a>
    </li>
    <li class="social-share-row__item">
        <a class="resp-sharing-button__link js-social-share" href="https://facebook.com/sharer/sharer.php?u=<?php echo esc_url( get_page_link() ); ?>" target="_blank" rel="noopener" aria-label="">
            <i class="icon-facebook"></i>
        </a>
    </li>
</ul>
<?php