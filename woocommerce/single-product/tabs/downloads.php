<ul>
    <?php if ($specsheet = get_field('spec_sheet_download') ) : ?>
        <li>
            <a href="<?php echo $specsheet['url']; ?>" target="_blank"><i class="icon-download"></i> <?php _e('Download Spec Sheet', 'gatorcases'); ?></a>
        </li>
    <?php endif; ?>
    <?php if ($measurement_guide = get_field('measurement_guide') ) : ?>
        <li>
            <a href="<?php echo $measurement_guide['url']; ?>" target="_blank"><i class="icon-download"></i> <?php _e('Download Measurement Guide', 'gatorcases'); ?></a>
        </li>
    <?php endif; ?>
    <?php if ($product_manual = get_field('product_manual') ) : ?>
        <li>
            <a href="<?php echo $product_manual['url']; ?>" target="_blank"><i class="icon-download"></i> <?php _e('Download Product Manual', 'gatorcases'); ?></a>
        </li>
    <?php endif; ?>
    <?php if ($product_flyer = get_field('product_flyer') ) : ?>
        <li>
            <a href="<?php echo $product_flyer['url']; ?>" target="_blank"><i class="icon-download"></i> <?php _e('Download Product Flyer', 'gatorcases'); ?></a>
        </li>
    <?php endif; ?>
</ul>
