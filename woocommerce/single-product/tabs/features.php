<ul>
    <?php while( have_rows('bullet_points') ) : the_row(); ?>
        <li><?php the_sub_field('product_point'); ?></li>
    <?php endwhile; ?>
</ul>
