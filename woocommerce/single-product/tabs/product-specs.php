<?php echo get_field('product_specification'); ?>

<ul>
    <?php if ($model = get_field('model')) : ?>
        <li><strong>Model:</strong> <?php echo $model; ?></li>
    <?php endif; ?>
    <?php if ($bar_code = get_field('bar_code')) : ?>
        <li><strong>UPC:</strong> <?php echo $bar_code; ?></li>
    <?php endif; ?>
    <?php if ($color = get_field('color')) : ?>
        <li><strong>Color:</strong> <?php echo $color; ?></li>
    <?php endif; ?>
    <?php if ($latches = get_field('latches')) : ?>
        <li><strong>Latches:</strong> <?php echo $latches; ?></li>
    <?php endif; ?>
    <?php if ($handles = get_field('handles')) : ?>
        <li><strong>Handles:</strong> <?php echo $handles; ?></li>
    <?php endif; ?>
    <?php if ($ata_rated = get_field('ata_rated')) : ?>
        <li><strong>ATA Accepted</strong> <?php echo $ata_rated; ?></li>
    <?php endif; ?>
    <?php if ($lockable = get_field('lockable')) : ?>
        <li><strong>Lockable:</strong> <?php echo $lockable == 'TRUE' ? 'Yes' : 'No'; ?></li>
    <?php endif; ?>
</ul>

<table class="table">
        <thead>
            <tr>
                <th></th>
                <th>Interior Dimensions</th>
                <th>Exterior Dimensions</th>
                <th>Shipping Dimensions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Length</th>
                <?php
                    $interior_length = get_field('interior_length');
                    $ext_length      = get_field('ext_length');
                    $shipping_length = get_field('_length');
                ?>
                <td><?php echo $interior_length ? $interior_length . '"' : '-'; ?></td>
                <td><?php echo $ext_length ? $ext_length . '"' : '-'; ?></td>
                <td><?php echo $shipping_length ? $shipping_length . '"' : '-'; ?></td>
            </tr>
            <tr>
                <th>Width</th>
                <?php
                    $interior_width = get_field('interior_width');
                    $ext_width      = get_field('ext_width');
                    $shipping_width = get_field('_width');
                ?>
                <td><?php echo $interior_width ? $interior_width . '"' : '-'; ?></td>
                <td><?php echo $ext_width ? $ext_width . '"' : '-'; ?></td>
                <td><?php echo $shipping_width ? $shipping_width . '"' : '-'; ?></td>
            </tr>
            <tr>
                <th>Height</th>
                <?php
                    $interior_height = get_field('interior_height');
                    $ext_height      = get_field('ext_height');
                    $shipping_height = get_field('_height');
                ?>
                <td><?php echo $interior_height ? $interior_height . '"' : '-'; ?></td>
                <td><?php echo $ext_height ? $ext_height . '"' : '-'; ?>"</td>
                <td><?php echo $shipping_height ? $shipping_height . '"' : '-'; ?></td>
            </tr>
            <tr>
                <th>Weight</th>
                <?php
                    $ext_weight      = get_field('ext_weight');
                    $shipping_weight = get_field('_weight');
                ?>
                <td></td>
                <td><?php echo $ext_weight ? $ext_weight . ' lbs' : '-'; ?></td>
                <td><?php echo $shipping_weight ? $shipping_weight . ' lbs' : '-'; ?></td>
            </tr>
        </tbody>
</table>

<?php if ($specsheet = get_field('spec_sheet_download') ) : ?>
<div>
    <a href="<?php echo $specsheet['url']; ?>" target="_blank"><i class="icon-download"></i> Download Spec Sheet</a>
</div>
<?php endif; ?>
