<div class="testimonial-content">
    <div class="testimonial-bg"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/testimonial-bg.jpg" alt=""></div>
    <div class="testimonial-title text-center">Product Testimonials</div>
    <h2 class="text-center">community</h2>
    <?php
        $args = array(
            'post_type' => 'testimonial',
            'posts_per_page' => -1,
            'orderby'       =>  'date',
            'order'         =>  'DESC',
            'meta_query'    =>  array(
                array(
                    'key'       => 'featured',
                    'value'     => 1
                ),
            )
        );

        $testimonials = new WP_Query($args);
    ?>

    <?php if ( $testimonials->have_posts() ) : ?>
        <div class="testimonial-slider">
            <?php while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
            <div class="testimonila-item">
                <div class="testimonial-text">
                    <?php echo get_field('testimonial_content', get_the_ID() ); ?>
                </div>
            </div>
            <?php endwhile; wp_reset_postdata() ?>
        </div>
    <?php endif; ?>
</div>
