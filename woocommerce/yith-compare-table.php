<?php
/**
 * Woocommerce Compare page
 *
 * @author  Your Inspiration Themes
 * @package YITH Woocommerce Compare
 * @version 1.1.4
 */

defined( 'YITH_WOOCOMPARE' ) || exit; // Exit if accessed directly.

global $product, $yith_woocompare;

?>
<?php
/**
 * Woocommerce Compare page
 *
 * @author  Your Inspiration Themes
 * @package YITH Woocommerce Compare
 * @version 1.1.4
 */

defined( 'YITH_WOOCOMPARE' ) || exit; // Exit if accessed directly.

global $product, $yith_woocompare;

?>

<div id="yith-woocompare" class="woocommerce">
	<section class="page-compare">
		<div class="container">
			<div class="compare-wrapper">
				<div class="compare-information">
					<div class="compare-top-info">
					<?php
						if ( is_shop() ) {
					?>
						<h2><?php woocommerce_page_title(); ?></h2>
					<?php
						} else {
					?>
					<h2><?php echo get_the_title(); ?></h2>
					<?php }?>
						<div class="back-wishlists"><a href="#"><i class="icon-link-arrow-left"></i> Back to previous page</a></div>
					</div>
				</div>

				<div class="compare-section-right">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-4">
							<div class="row">
								<?php foreach ( $products as $product_id => $product ) : ?>
									<div class="col-sm-6">
										<div class="compare-main">
											<?php if ( $show_image ) : ?>
												<div class="compare-box">
													<div class="compare-img-box">
														<div class="compare-img">
															<a href="<?php echo esc_attr( $product->get_permalink() );?>">
																<?php echo fx_get_image_tag( get_post_thumbnail_id( $product->get_id() ) , 'masthead-slide__img', 'full', false, $product->get_title() ); ?>
															</a>     
														</div>
													</div>
												</div>
											<?php endif; ?>
											<div class="compare-description">
												<p><?php echo esc_html( $product->get_title() ); ?><?php echo wp_kses_post( $product->fields['price'] ); ?></p>
												<?php if ( get_field('online_dealers', $product->get_id() ) ) : ?>
													<div class="com-bttn"><a href="<?php echo get_field('online_dealers', $product->get_id() ); ?>" class="btn btn-primary">Online Dealers</a></div>
												<?php endif; ?>
												<?php if ( get_field('local_dealers', $product->get_id() ) ) : ?>
													<div class="com-bttn"><a href="<?php echo get_field('online_dealers', $product->get_id() ); ?>" class="btn btn-primary">Local Dealers</a></div>
												<?php endif; ?>
												
												<div class="compare">
													<a href="<?php echo esc_url( $yith_woocompare->obj->remove_product_url( $product_id ) ); ?>"
														data-iframe="<?php echo esc_attr( $iframe ); ?>"
														data-product_id="<?php echo esc_attr( $product_id ); ?>"><?php esc_html_e( 'Remove', 'yith-woocommerce-compare' ); ?>
													</a>
												</div>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<section class="compare-table-section">
		<div class="container">
			<div class="table-compare">
				<?php
				if ( empty( $products ) ) :
					echo '<div class="container"><p>' . esc_html( apply_filters( 'yith_woocompare_empty_compare_message', __( 'No products added in the comparison table.', 'yith-woocommerce-compare' ) ) ) . '</p></div>';
				else :
					?>

					<?php do_action( 'yith_woocompare_before_main_table', $products, $fixed ); ?>

					<table class="<?php echo empty($products) ? 'empty-list' :'';?>">
						<tbody>

						<?php foreach ( $fields as $field => $name ) : ?>

							<tr class="<?php echo ! in_array( $field, $different, true ) ? esc_attr( $field ) : esc_attr( $field ) . ' different'; ?>">

								<th>
									<?php echo esc_html( $name ); ?>
								</th>

								<?php
								$index = 0;
								foreach ( $products as $product_id => $product ) :
									// Set td class.
									$product_class = ( ( 0 === ( $index % 2 ) ) ? 'odd' : 'even' ) . ' product_' . $product_id;
									if ( 'stock' === $field ) {
										$availability   = $product->get_availability();
										$product_class .= ' ' . ( empty( $availability['class'] ) ? 'in-stock' : $availability['class'] );
									}
									?>

									<td class="<?php echo esc_attr( $product_class ); ?>">
										<?php
										switch ( $field ) {

											case 'product_info':

												if ( $product->is_type( 'bundle' ) ) {
													$bundled_items = $product->get_bundled_items();

													if ( ! empty( $bundled_items ) ) {

														echo '<div class="bundled_product_list">';

														foreach ( $bundled_items as $bundled_item ) {
															// wc_bundles_bundled_item_details hook.
															do_action( 'wc_bundles_bundled_item_details', $bundled_item, $product );
														}

														echo '</div>';
													}
												}

												if ( shortcode_exists( 'yith_ywraq_button_quote' ) && $product->is_type( 'simple' ) && 'yes' === $show_request_quote ) {
													echo do_shortcode( '[yith_ywraq_button_quote product="' . $product->get_id() . '"]' );
												}

												break;

											case 'rating':
												$rating = function_exists( 'wc_get_rating_html' ) ? wc_get_rating_html( $product->get_average_rating() ) : $product->get_rating_html();
												echo $rating ? '<div class="woocommerce-product-rating">' . wp_kses_post( $rating ) . '</div>' : '-';
												break;

											default:
												if ( $product instanceof WC_Product_Variation && apply_filters( 'yith_woocompare_support_show_single_variations', true ) ) {
													$parent_product    = wc_get_product( $product->get_parent_id() );
													$attributes        = $product->get_attributes();
													$parent_attributes = wp_get_post_terms( $product->get_parent_id(), $field );

													if ( strpos( $field, 'pa_' ) !== false ) {

														if ( isset( $attributes[ $field ] ) && '' !== $attributes[ $field ] ) {
															$attributes[ $field ];
														} elseif ( ! empty( $parent_attributes ) ) {
															$n_attributes = count( $parent_attributes );
															$i            = 1;
															foreach ( $parent_attributes as $attribute ) {
																echo esc_html( $attribute->name . ' ' );
																if ( $i < $n_attributes ) {
																	echo ', ';
																}
																$i ++;
															}
														} else {
															echo '-';
														}
													} else {
														echo wp_kses_post( apply_filters( 'yith_woocompare_single_variation_field_value', do_shortcode( $product->fields[ $field ] ), $product, $field ) );
													}
												} else {
													echo apply_filters( 'yith_woocompare_value_default_field', empty( $product->fields[ $field ] ) ? '-' : do_shortcode( $product->fields[ $field ] ), $product, $field ) ;
												}
												break;
										}
										?>
									</td>
									<?php
									++ $index;
								endforeach
								?>
							</tr>
						<?php endforeach; ?>

						</tbody>
					</table>
				<?php endif; ?>
			</div>
		</div>			
	</section>
</div>